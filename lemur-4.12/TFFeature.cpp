/*
 * TFFeature.cpp
 *
 *  Created on: Jan 1, 2017
 *      Author: vandermonde
 */

#include "TFFeature.h"

wink::TFFeatureQueryRep::TFFeatureQueryRep(const TermQuery &qry, const Index &dbIndex, double *idfValue, TFIDFParameter::WeightParam &param): ArrayQueryRep (dbIndex.termCountUnique()+1, qry, dbIndex), ind(dbIndex), idf(idfValue), prm(param)
{
  startIteration();
  while (hasMore()) {
    QueryTerm *qt = nextTerm();
    setCount(qt->id(), queryTFWeight(qt->weight())*idf[qt->id()]);
    // cout << "term : "<< dbIndex.term(qt->id()) << " idf="<< idf[qt->id()] <<    " total "<< dbIndex.docCount() << " with term "<< dbIndex.docCount(qt->id()) << endl;
    delete qt;
  }
}
double wink::TFFeatureQueryRep::queryTFWeight(const double rawTF) const
{
  if (prm.tf == TFIDFParameter::RAWTF) {
    return (rawTF);
  } else if (prm.tf == TFIDFParameter::LOGTF) {
    return (log(rawTF+1));
  } else if (prm.tf == TFIDFParameter::BM25) {
    return (TFIDFRetMethod::BM25TF(rawTF,prm.bm25K1,0,
                                   1, 1));  // no length normalization for query
  } else {  // default to raw TF
    cerr << "Warning: unknown TF method, raw TF assumed\n";
    return rawTF;
  }
}



double wink::TFFeatureDocRep::docTFWeight(const double rawTF) const
{
  if (prm.tf == TFIDFParameter::RAWTF) {
    return (rawTF);
  } else if (prm.tf == TFIDFParameter::LOGTF) {
    return (log(rawTF+1));
  } else if (prm.tf == TFIDFParameter::BM25) {

    return (TFIDFRetMethod::BM25TF(rawTF, prm.bm25K1, prm.bm25B,
                                   docLength, ind.docLengthAvg()));
  } else {  // default to raw TF
    cerr << "Warning: unknown TF method, raw TF assumed\n";
    return rawTF;
  }
}


feature::TFFeatureMethod::TFFeatureMethod(const Index &dbIndex, ScoreAccumulator &accumulator) :TFIDFRetMethod(dbIndex, accumulator)
{
  // set default parameter value
  docTFParam.tf = TFIDFParameter::RAWTF;
  docTFParam.bm25K1 = TFIDFParameter::defaultDocK1;
  docTFParam.bm25B = TFIDFParameter::defaultDocB;

  qryTFParam.tf = TFIDFParameter::RAWTF;
  qryTFParam.bm25K1 = TFIDFParameter::defaultQryK1;
  qryTFParam.bm25B = TFIDFParameter::defaultQryB;

  fbParam.howManyTerms = TFIDFParameter::defaultHowManyTerms;
  fbParam.posCoeff = TFIDFParameter::defaultPosCoeff;

  // pre-compute IDF values
  idfV = new double[dbIndex.termCountUnique()+1];
  for (COUNT_T i=1; i<=dbIndex.termCountUnique(); i++) {
    idfV[i] = log((dbIndex.docCount()+1)/(0.5+dbIndex.docCount(i)));
  }
  scFunc = new ScoreFunction();
}



feature::IDFFeatureMethod::IDFFeatureMethod(const Index &dbIndex, ScoreAccumulator &accumulator) :TFIDFRetMethod(dbIndex, accumulator)
{
  // set default parameter value
  docTFParam.tf = TFIDFParameter::RAWTF;
  docTFParam.bm25K1 = TFIDFParameter::defaultDocK1;
  docTFParam.bm25B = TFIDFParameter::defaultDocB;

  qryTFParam.tf = TFIDFParameter::RAWTF;
  qryTFParam.bm25K1 = TFIDFParameter::defaultQryK1;
  qryTFParam.bm25B = TFIDFParameter::defaultQryB;

  fbParam.howManyTerms = TFIDFParameter::defaultHowManyTerms;
  fbParam.posCoeff = TFIDFParameter::defaultPosCoeff;

  // pre-compute IDF values
  idfV = new double[dbIndex.termCountUnique()+1];
  for (COUNT_T i=1; i<=dbIndex.termCountUnique(); i++) {
    idfV[i] = log((dbIndex.docCount()+1)/(0.5+dbIndex.docCount(i)));
  }
  scFunc = new ScoreFunction();
}
