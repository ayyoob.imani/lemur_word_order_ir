/*
 * WOrdrdOkapiRetMethod.h
 *
 *  Created on: Jul 5, 2017
 *      Author: vandermonde
 */

#ifndef WORDRDOKAPIRETMETHOD_H_
#define WORDRDOKAPIRETMETHOD_H_

#include <OkapiRetMethod.hpp>
#include <TextQueryRep.hpp>
#include <DocumentRep.hpp>
#include <MatchInfo.hpp>
#include <vector>

using namespace lemur::api;

namespace vandermonde {

class cacheItem {
public:
	TERMID_T first;
	TERMID_T second;
	double MI;
};

class MatchInfo {
public:
	TERMID_T tid;
	int freq;
	LOC_T* poses;
	const lemur::api::QueryTerm *qTerm;

	~MatchInfo(){
		delete poses;
	}

};

class MatchedTermInfo {
public:
	TERMID_T tid;
	int freq;
	const LOC_T* poses;
	const lemur::api::QueryTerm *qTerm;
};


class WOrderOkapiScoreFunc: public lemur::retrieval::OkapiScoreFunc {
public:
	WOrderOkapiScoreFunc(const lemur::api::Index &dbIndex, int windowSize) :
			OkapiScoreFunc(dbIndex) {
		SINGLE_TERM_SCORE = 0.5;
		QUERY_REVERT_TERM_SCORE = 0.5;
		cacheSize = 500;
		this->windowSize = windowSize;
	}
	~WOrderOkapiScoreFunc() {
	}

	virtual double adjustedScore2(double origScore, const TextQueryRep *qRep,
			const DocumentRep *dRep, double coeff) const;
protected:

	double SINGLE_TERM_SCORE;
	double QUERY_REVERT_TERM_SCORE;
	int cacheSize ;
	int windowSize;

	void intersectDocs(const LOC_T* firstPoses, int firstCount,
			const LOC_T* secPoses, int secCount, int* intersects) const;
	double ordered_MI(int first, int sec) const;
	bool areInOrder(const LOC_T* firstPoses, int firstCount,
			const LOC_T* secPoses, int secCount) const;

	bool incache(int first, int sec, double &res) const;
	void addtocache(int first, int sec, double res) const;
	bool inIdfcache(int first) const;
	void addtoIdfcache(int first) const;

	mutable std::vector<cacheItem> cache;
	mutable std::vector<int> idfCache;

};

/// OkapiQueryRep carries an array to store the count of relevant docs with a term
class WOrderOkapiQueryRep: public lemur::retrieval::OkapiQueryRep {
public:

	WOrderOkapiQueryRep(const lemur::api::TermQuery &qry,
			const lemur::api::Index &dbIndex, double paramK3) :
			OkapiQueryRep(qry, dbIndex, paramK3) {
		query = dynamic_cast<const TermQuery *>(&qry);
	}

	const lemur::api::TermQuery* query;
};

class WOrdrdOkapiRetMethod: public lemur::retrieval::OkapiRetMethod {
public:
	WOrdrdOkapiRetMethod(const Index &dbIndex,
			lemur::api::ScoreAccumulator &accumulator, int windowSize);

	virtual ~WOrdrdOkapiRetMethod();

	virtual lemur::api::ScoreFunction *scoreFunc();

	virtual lemur::api::TextQueryRep *computeTextQueryRep(
			const lemur::api::TermQuery &qry) {
		return (new WOrderOkapiQueryRep(qry, ind, tfParam.k3));
	}

protected:
	WOrderOkapiScoreFunc *WOAdjFunc;

};

inline lemur::api::ScoreFunction *WOrdrdOkapiRetMethod::scoreFunc() {
	return WOAdjFunc;
}

}
#endif /* WORDRDOKAPIRETMETHOD_H_ */
