/*
 * WOrdrdOkapiRetMethod.cpp
 *
 *  Created on: Jul 5, 2017
 *      Author: vandermonde
 */

#include "ModOkapi.h"
#include <MatchInfo.hpp>

namespace vandermonde {

ModOkapiRetMethod::ModOkapiRetMethod(const Index &dbIndex,
		ScoreAccumulator &accumulator) :
		OkapiRetMethod(dbIndex, accumulator) {

	WOAdjFunc = new ModOkapiScoreFunc(dbIndex);

}

ModOkapiRetMethod::~ModOkapiRetMethod() {
	delete WOAdjFunc;
}

double ModOkapiScoreFunc::adjustedScore(double origScore,
		const TextQueryRep *qRepn, const DocumentRep *dRep) const {
	DOCID_T did = dRep->getID();

	lemur::api::TermInfoList *tList = ind.termInfoList(did);
	lemur::api::TermInfo *info;

	ModOkapiQueryRep* qRep = (ModOkapiQueryRep*) qRepn;
	qRep->query->startTermIteration();
	int querySize = 0;
	while (qRep->query->hasMore()) {

		TERMID_T tid = ind.term(qRep->query->nextTerm()->spelling());
		tList->startIteration();
		while (tList->hasMore()) {
			info = tList->nextEntry();

			if (tid == info->termID()) {
				querySize++;			}
		}
	}

	if (querySize == 1)
		origScore = -500;
	if (querySize == 0)
		origScore = -1000;

	delete tList;
	return origScore;
}

}
