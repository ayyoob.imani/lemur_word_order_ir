/*
 * utility.h
 *
 *  Created on: Dec 29, 2016
 *      Author: vandermonde
 */

#ifndef UTILITY_H_
#define UTILITY_H_

#include <string>
#include <cctype> // for isspace


using namespace std;

namespace myutility{
	string left_trim(string str);
	string right_trim(string str);
	string trim(string str);
};


#endif /* UTILITY_H_ */
