/*
 * JudgmentReader.cpp
 *
 *  Created on: Dec 29, 2016
 *      Author: vandermonde
 */

#include "JudgmentReader.h"


JudgmentReader::JudgmentReader(const std::string & file_name) {

	string line;

	std::string item;
	ifstream mfile;
	mfile.open(file_name.c_str());
	if (mfile.is_open()) {
		while (getline(mfile, line)) {
			line = trim(line);
			if (!line.empty()) {
				Judgment judgment;
				std::stringstream ss;
				ss.str(line);

				std::getline(ss, item, ' ');
				judgment.q_id = atoi(item.c_str());
				//omit one line because it is redundunt if file
				std::getline(ss, item, ' ');
				std::getline(ss, judgment.doc_id, ' ');
				std::getline(ss, item, ' ');
				judgment.rel = atoi(item.c_str());

				auto docs = data_by_qid.find(judgment.q_id);
				if(docs != data_by_qid.end()){
					(*((map<string, bool>*) (&docs->second)))[judgment.doc_id] = judgment.rel;
				} else {
					map<string, bool> dcs = {{judgment.doc_id, judgment.rel}};
					data_by_qid[judgment.q_id] = dcs;
				}
			}
		}

	} else {
		string cause;
		cause.append("file not found at: ");
		cause.append(file_name);
		cout<<cause.c_str();
		exit(10);
	}
}


JudgmentReader::~JudgmentReader() {
// TODO Auto-generated destructor stub
}

