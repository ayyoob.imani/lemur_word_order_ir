/*
 * TFFeature.h
 *
 *  Created on: Jan 1, 2017
 *      Author: vandermonde
 */

#ifndef TFFEATURE_H_
#define TFFEATURE_H_

#include <TFIDFRetMethod.hpp>

using namespace lemur;
using namespace lemur::retrieval;
using namespace api;

namespace wink {
class TFFeatureQueryRep: public ArrayQueryRep {
public:
	TFFeatureQueryRep(const lemur::api::TermQuery &qry,
			const lemur::api::Index &dbIndex, double *idfValue,
			TFIDFParameter::WeightParam &param);

	virtual ~TFFeatureQueryRep() {
	}

	double queryTFWeight(const double rawTF) const;
protected:
	TFIDFParameter::WeightParam &prm;
	double *idf;
	const lemur::api::Index &ind;
};

/// Representation of a doc (as a weighted vector) in the TFIDF method
class TFFeatureDocRep: public lemur::api::DocumentRep {
public:
	TFFeatureDocRep(lemur::api::DOCID_T docID, const lemur::api::Index &dbIndex,
			double *idfValue, TFIDFParameter::WeightParam &param) :
			lemur::api::DocumentRep(docID, dbIndex.docLength(docID)), ind(
					dbIndex), prm(param), idf(idfValue) {
	}
	virtual ~TFFeatureDocRep() {
	}
	virtual double termWeight(lemur::api::TERMID_T termID,
			const lemur::api::DocInfo *info) const {
		return (docTFWeight(info->termCount()));
	}
	virtual double scoreConstant() const {
		return 0;
	}

	double docTFWeight(const double rawTF) const;
private:

	const lemur::api::Index & ind;
	TFIDFParameter::WeightParam &prm;
	double *idf;
};


/// Representation of a doc (as a weighted vector) in the TFIDF method
class IDFFFeatureDocRep: public lemur::api::DocumentRep {
public:
	IDFFFeatureDocRep(lemur::api::DOCID_T docID, const lemur::api::Index &dbIndex,
			double *idfValue, TFIDFParameter::WeightParam &param) :
			lemur::api::DocumentRep(docID, dbIndex.docLength(docID)), ind(
					dbIndex), prm(param), idf(idfValue) {
	}
	virtual ~IDFFFeatureDocRep() {
	}
	virtual double termWeight(lemur::api::TERMID_T termID,
			const lemur::api::DocInfo *info) const {
		return (idf[termID]);
	}
	virtual double scoreConstant() const {
		return 0;
	}

//	double docTFWeight(const double rawTF) const;
private:

	const lemur::api::Index & ind;
	TFIDFParameter::WeightParam &prm;
	double *idf;
};
}

namespace feature {
class TFFeatureMethod: public lemur::retrieval::TFIDFRetMethod {
public:

	TFFeatureMethod(const lemur::api::Index &dbIndex,
			lemur::api::ScoreAccumulator &accumulator);
	virtual ~TFFeatureMethod() {
		delete[] idfV;
		delete scFunc;
	}

	virtual lemur::api::TextQueryRep *computeTextQueryRep(
			const lemur::api::TermQuery &qry) {
		return (new wink::TFFeatureQueryRep(qry, ind, idfV, qryTFParam));
	}

	virtual lemur::api::DocumentRep *computeDocRep(lemur::api::DOCID_T docID) {
		return (new wink::TFFeatureDocRep(docID, ind, idfV, docTFParam));
	}

};

class IDFFeatureMethod: public lemur::retrieval::TFIDFRetMethod {
public:

	IDFFeatureMethod(const lemur::api::Index &dbIndex,
			lemur::api::ScoreAccumulator &accumulator);

	virtual lemur::api::TextQueryRep *computeTextQueryRep(
			const lemur::api::TermQuery &qry) {
		return (new wink::TFFeatureQueryRep(qry, ind, idfV, qryTFParam));
	}

	virtual lemur::api::DocumentRep *computeDocRep(lemur::api::DOCID_T docID) {
		return (new wink::IDFFFeatureDocRep(docID, ind, idfV, docTFParam));
	}

};
}
#endif /* TFFEATURE_H_ */
