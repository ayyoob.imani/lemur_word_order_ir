/*
 * WOrdrdOkapiRetMethod.h
 *
 *  Created on: Jul 5, 2017
 *      Author: vandermonde
 */

#ifndef MODOKAPI_H_
#define MODOKAPI_H_

#include <OkapiRetMethod.hpp>
#include <TextQueryRep.hpp>
#include <DocumentRep.hpp>
#include <MatchInfo.hpp>
#include <vector>

using namespace lemur::api;

namespace vandermonde {


class ModOkapiScoreFunc: public lemur::retrieval::OkapiScoreFunc {
public:
	ModOkapiScoreFunc(const lemur::api::Index &dbIndex) :
			OkapiScoreFunc(dbIndex) {
	}
	~ModOkapiScoreFunc() {
	}

	virtual double adjustedScore(double origScore, const TextQueryRep *qRep,
			const DocumentRep *dRep) const;

};

/// OkapiQueryRep carries an array to store the count of relevant docs with a term
class ModOkapiQueryRep: public lemur::retrieval::OkapiQueryRep {
public:

	ModOkapiQueryRep(const lemur::api::TermQuery &qry,
			const lemur::api::Index &dbIndex, double paramK3) :
			OkapiQueryRep(qry, dbIndex, paramK3) {
		query = dynamic_cast<const TermQuery *>(&qry);
	}

	const lemur::api::TermQuery* query;
};

class ModOkapiRetMethod: public lemur::retrieval::OkapiRetMethod {
public:
	ModOkapiRetMethod(const Index &dbIndex,
			lemur::api::ScoreAccumulator &accumulator);

	virtual ~ModOkapiRetMethod();

	virtual lemur::api::ScoreFunction *scoreFunc();

	virtual lemur::api::TextQueryRep *computeTextQueryRep(
			const lemur::api::TermQuery &qry) {
		return (new ModOkapiQueryRep(qry, ind, tfParam.k3));
	}

protected:
	ModOkapiScoreFunc *WOAdjFunc;

};

inline lemur::api::ScoreFunction *ModOkapiRetMethod::scoreFunc() {
	return WOAdjFunc;
}

}
#endif /* WORDRDOKAPIRETMETHOD_H_ */
