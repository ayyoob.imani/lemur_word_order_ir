/*
 * worder.cpp
 *
 *  Created on: Jul 5, 2017
 *      Author: vandermonde
 */
#include <stdio.h>
#include <stdlib.h>
#include <Index.hpp>
#include <IndexManager.hpp>
#include <DocStream.hpp>
#include <BasicDocStream.hpp>
#include <ScoreAccumulator.hpp>
#include <RetMethodManager.hpp>
#include <ResultFile.hpp>
#include <iostream>
#include <fstream>

#include "WOrdrdOkapiRetMethod.h"
#include "DataExtraction.h"

using namespace lemur;
using namespace api;
using namespace parse;
using namespace retrieval;

void tfIdfRetrieval(int windowSize, double coeff);


namespace LocalParameter
{
	int windowSize;

	// Number of documents to be re-ranked
	int RerankedDocCount;

	// 0 : apply plm on single term query; 1 : do not apply plm on single term query
	int IgnoreSingleTermQuery;

	int doItOrdered;

	double coeff;

	void get()
	{

		windowSize = ParamGetInt("OrderWindowSize", 5);

		RerankedDocCount = ParamGetInt("RerankedDocCount", 2000);

		IgnoreSingleTermQuery = ParamGetInt("IgnoreSingleTermQuery", 0);

		doItOrdered = ParamGetInt("orderedPLM", 0);
		coeff = ParamGetDouble("coeff", 1.0);
	}
};


void GetAppParam()
{
	RetrievalParameter::get();

	SimpleKLParameter::get();

	LocalParameter::get();
}



int AppMain(int argc, char*  argv[]) {

//	vandermonde::DataExtraction::extractAndWriteData();

	tfIdfRetrieval(LocalParameter::windowSize, LocalParameter::coeff);
	return EXIT_SUCCESS;
}


void tfIdfRetrieval(int windowSize, double coeff) {

	Index *dbIndex = IndexManager::openIndex(RetrievalParameter::databaseIndex);

	ResultFile woResFile;
	std::filebuf wofb;
	std::ostream woos(&wofb);
	if(LocalParameter::doItOrdered){
		wofb.open(RetrievalParameter::resultFile.c_str(), std::ios::out);
		woResFile.openForWrite(woos, *dbIndex);
	}

	std::filebuf okfb;
	okfb.open("resultsok/kl.txt", std::ios::out);
	std::ostream okos(&okfb);
	ResultFile okResFile;
	okResFile.openForWrite(okos, *dbIndex);

	DocStream *queryStream = new BasicDocStream(
			RetrievalParameter::textQuerySet);

	ArrayAccumulator wOrderOkapiAccumulator(dbIndex->docCount());
	ArrayAccumulator okapiAccumulator(dbIndex->docCount());

	vandermonde::WOrdrdOkapiRetMethod *WOrderedMethod = new vandermonde::WOrdrdOkapiRetMethod(
			*dbIndex, wOrderOkapiAccumulator, windowSize);
	RetrievalMethod *okapiMethod = new OkapiRetMethod(*dbIndex,
			okapiAccumulator);

	IndexedRealVector wOrderedResults;
	IndexedRealVector okapiResults;

	queryStream->startDocIteration();
	TextQuery *q;
	int count = 51;
	while (queryStream->hasMore()) {

		cout << "round: " << count++ << endl;

		Document *d = queryStream->nextDoc(); // fetch the query document

		q = new TextQuery(*d);

		QueryRep * wOrderedqr = WOrderedMethod->computeQueryRep(*q); // compute the query representation

		WOrderedMethod->scoreCollection(*wOrderedqr, wOrderedResults);
		wOrderedResults.Sort();
		okResFile.writeResults(q->id(),&wOrderedResults,1000);

		if(LocalParameter::doItOrdered){
			IndexedRealVector::iterator itt = wOrderedResults.begin();
			for(int ii=0; ii<2000 && itt != wOrderedResults.end(); ii++, itt++){
				const DocumentRep * drr = WOrderedMethod->computeDocRep((*itt).ind);
				(*itt).val = ((vandermonde::WOrderOkapiScoreFunc*)(WOrderedMethod->scoreFunc()))->adjustedScore2((*itt).val,
						dynamic_cast<TextQueryRep*> (wOrderedqr), drr, coeff);
			}

			wOrderedResults.Sort();
			woResFile.writeResults(q->id(), &wOrderedResults, 1000);
		}

	}

	okfb.close();
	if(LocalParameter::doItOrdered){
		wofb.close();
	}
}
