#include "utility.h"

using namespace std;

int count_ones(int arr[], int arr_length) {
	int result = 0;
	for (int i = 0; i < arr_length; i++) {
		if (arr[i] == 1)
			result++;
	}
	return result;
}

double calculate_weighted_average(vector<double> values, vector<int> weights) {
	if (values.size() != weights.size())
		throw "calculate_weighted_average: values and weights must have the same size.";

	double weighted_sum = 0;
	double sum_of_weights = 0;

	for (int i = 0; i < values.size(); i++) {
		weighted_sum += values[i] * weights[i];
		sum_of_weights += weights[i];
	}

	return weighted_sum / sum_of_weights;
}