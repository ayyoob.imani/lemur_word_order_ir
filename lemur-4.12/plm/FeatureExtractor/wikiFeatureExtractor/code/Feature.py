class Feature:
    def __init__(self, type, terms, count=0):
        self.type = type
        self.terms = terms
        self.count = count

    unigram = "UWF"
    bigram = "BWF"
    proximity = "WPRX"

    def __eq__(self, other):
        return self.type == other.type and self.terms == other.terms

    def __repr__(self):
        return "(" + self.type + ", " + str(self.terms) + ", " + str(self.count) + ")"

    def to_formal_string(self):
        return self.type + " " + ",".join(self.terms) + " " + str(self.count)


