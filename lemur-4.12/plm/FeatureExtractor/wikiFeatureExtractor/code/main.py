from Query import *
from FeatureExtractor import *
from FeatureMatcher import *


if __name__ == "__main__":
    parser = QueryParser("./data/trec_ap_fixed_stemmed.txt")
    matcher = FeatureMatcher("./data/data.txt")
    print "done loading"
    matcher.count_all_and_write_to_file(
        FeatureExtractor.extract_from_queries(parser.get_all_queries()),
        "./output/out.txt"
    )


# PR is only for "two-word" situations.
# every DOC in query file is a query
