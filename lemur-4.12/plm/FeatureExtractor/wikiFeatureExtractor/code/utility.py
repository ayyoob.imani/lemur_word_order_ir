import datetime

def is_ascii(string):
    return all(ord(c) < 128 for c in string)


def extend_unique(list1, list2):
    for item in list2:
        if item not in list1:
            list1.append(item)

def append_unique(_list, item):
    if item not in _list:
        _list.append(item)


class Timer:
    def __int__(self):
        self.time = None

    def record(self):
        self.time = datetime.datetime.now()

    def print_elapsed(self, message = ""):
        print message, (datetime.datetime.now() - self.time)