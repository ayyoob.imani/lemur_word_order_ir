import re

class QueryParser:
    def __init__(self, file_name):
        self.source = open(file_name).read().splitlines()
        self.source_len = len(self.source)
        self.index = 0

    def get_next_query(self):

        next_line = self.get_next_line()

        if next_line is None:
            return None

        match = re.match(r'<DOC (\d*)>', next_line)
        if match is None:
            raise RuntimeError("invalid start of query: " + next_line)
        number = match.group(1)

        word_list = []
        next_line = self.get_next_line()
        while next_line != "</DOC>":
            if next_line is None:
                raise RuntimeError("no end of query found for last query in file")
            word_list.append(next_line)
            next_line = self.get_next_line()

        return Query(number, word_list)

    def get_next_line(self):
        if self.index >= self.source_len:
            return None

        while self.source[self.index].strip() == "" and self.index < self.source_len:
            self.index += 1

        self.index += 1
        return self.source[self.index - 1]

    def get_all_queries(self):
        result = []
        query = self.get_next_query()
        while query is not None:
            result.append(query)
            query = self.get_next_query()
        self.index = 0 #re-initialize
        return result


class Query:
    def __init__(self, number, word_list):
        self.number = number
        self.word_list = word_list
        # self.word_set = frozenset(word_list)

    def __repr__(self):
        return self.number + ", (" + ", ".join(self.word_list) + ")"
