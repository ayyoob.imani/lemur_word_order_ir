from Feature import *
from utility import *

class FeatureMatcher:
    def __init__(self, filename):
        self.data = [title.split("_") for title in open(filename, "r").read().splitlines()]
        self.window_size = 5

    def count_unigram_occurences(self, unigram):
        result = 0
        for line in self.data:
            if self.matches_unigram(line, unigram):
                result += 1
        return result

    def matches_unigram(self, line, unigram):
        return unigram in line

    def count_bigram_occurences(self, word1, word2):
        result = 0
        for line in self.data:
            if self.matches_bigram(line, word1, word2):
                result += 1
        return result

    def matches_bigram(self, line, word1, word2):
        for i in xrange(len(line) - 1):
            if line[i] == word1 and line[i + 1] == word2:
                # print "matched with", line
                return True
        return False

    def count_proximity_occurences(self, word1, word2):
        result = 0
        for line in self.data:
            if self.matches_proximity(line, word1, word2):
                result += 1
        return result

    def matches_proximity(self, line, word1, word2):
        for i in xrange(len(line)):
            if line[i] == word1:
                for j in xrange(max(i - self.window_size + 1, 0), min(i + self.window_size, len(line) - 1)):
                    if line[j] == word2:
                        # print "match :", line, i, j
                        return True
        return False


    def count_all_and_write_to_file(self, features, out_file_name):
        timer = Timer()
        timer.record()
        try:
            for i, line in enumerate(self.data):
                if i % 10000 == 0:
                    print "line ", i
                    timer.print_elapsed("time since last 10000: ")
                    timer.record()

                for feature in features[Feature.unigram]:
                    if self.matches_unigram(line, feature.terms[0]):
                        feature.count += 1

                for feature in features[Feature.bigram]:
                    if self.matches_bigram(line, feature.terms[0], feature.terms[1]):
                        feature.count += 1

                for feature in features[Feature.proximity]:
                    if self.matches_proximity(line, feature.terms[0], feature.terms[1]):
                        feature.count += 1

        except KeyboardInterrupt:
            pass

        print "done counting. starting writing to file."
        print out_file_name
        print features
        file = open(out_file_name, "w")
        for feature_type in features:
            for feature in features[feature_type]:
                file.write(feature.to_formal_string() + "\n")


