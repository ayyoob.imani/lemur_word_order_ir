from nltk.stem import PorterStemmer
from utility import *

stop_words = frozenset(['I', 'a', 'about', 'an', 'are', 'as', 'at', 'be', 'by', 'com',
                       'for', 'from', 'how', 'in', 'is', 'it', 'of', 'on', 'or', 'that',
                       'the', 'this', 'to', 'was', 'what', 'when', 'where', 'who', 'will',
                       'with', 'the', 'www'])


def remove_noise(string):
    noise_list = ['!', '`', '@', '#', '$', '%', '^', '&', '*', '(', ')',
                  '-', '+', '=', '/', '"', '[', ']', '{', '}', '|',
                  '\\', '?', ',', '...']
    return remove_from_string(string, noise_list)

def remove_from_string(string, list):
    result = string
    for item in list:
        result = result.replace(item, '')
    return result

def neatify_file(src, dst):
    in_file = open(src, "r")
    out_file = open(dst, "w")

    stemmer = PorterStemmer()

    line = in_file.readline()
    line_count = 1
    while line != "":
        line_count += 1
        if line_count % 100000 == 0:
            print line_count / 100000, " out of ", 138

        #remove noise and extra space
        words_in_line = remove_noise(line).strip().split('_')

        #remove non-ascii
        words_in_line = filter(is_ascii, words_in_line)

        # for i, word in enumerate(words_in_line):
        # for i in range(len(words_in_line)):
        #     word = words_in_line[i]
        #     print word, " is : ",
        #     if is_ascii(word) is False:
        #         print "non-ascii"
        #         del words_in_line[i]
        #         i -= 1
        #     else:
        #         print "ascii"

        #stem
        words_in_line = map(stemmer.stem, words_in_line)

        #stop-words-check
        words_in_line = filter(lambda word : word not in stop_words, words_in_line)

        # print "giving set ", words_in_line, " to stemmer"
        # for i, word in enumerate(words_in_line):
        #     word = stemmer.stem(word)
        #     if word in stop_words:
        #         del words_in_line[i]

        neatified_line = '_'.join(words_in_line) + "\n"

        if neatified_line != "\n":
            out_file.write(neatified_line)

        line = in_file.readline()