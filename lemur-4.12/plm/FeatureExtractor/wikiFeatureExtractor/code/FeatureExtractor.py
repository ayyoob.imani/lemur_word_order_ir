from Feature import *
from utility import *

class FeatureExtractor:
    @staticmethod
    def extract_from_queries(queries):
        # print "queries passed to extractor are :"
        # print queries
        result = {Feature.unigram: [], Feature.bigram: [], Feature.proximity: []}
        for query in queries:
            query_features = FeatureExtractor.extract_from_query(query)
            extend_unique(result[Feature.unigram], query_features[Feature.unigram])
            extend_unique(result[Feature.bigram], query_features[Feature.bigram])
            extend_unique(result[Feature.proximity], query_features[Feature.proximity])
        # print "resultant feature set is: "
        # print result
        return result

    @staticmethod
    def extract_from_query(query):
        result = {Feature.unigram: [], Feature.bigram: [], Feature.proximity: []}

        for word in query.word_list:
            append_unique(result[Feature.unigram], Feature(Feature.unigram, [word]))

        n_words = len(query.word_list)
        for i in xrange(n_words - 1):
            append_unique(result[Feature.bigram], Feature(Feature.bigram, [query.word_list[i], query.word_list[i+1]]))

        for i in xrange(n_words):
            for j in xrange(i + 1, n_words):
                append_unique(result[Feature.proximity], Feature(Feature.proximity, [query.word_list[i], query.word_list[j]]))

        return result