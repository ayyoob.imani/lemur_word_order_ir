//
// PLMRetEval 
//
// 13 November 2010 -- Yuanhua Lv @ UIUC (ylv2@uiuc.edu)
// 
//

#include <iomanip>

#include "common_headers.hpp"
#include "IndexManager.hpp"
#include "BasicDocStream.hpp"
#include "RetMethodManager.hpp"
#include "ResultFile.hpp"
#include "RelDocUnigramCounter.hpp"
#include "FeatureCache.h"
#include <optim.hpp>
#include <chrono>
#include <pthread.h>

#include "optimization/MultivariateRealFunction.h"
#include "optimization/SAOptimizer.h"
#include "optimization/CoarseFineOptimizer.h"
#include "utility.h"

using namespace lemur::api;
using namespace lemur::retrieval;

const double pi = 3.14159265358979323846;
static vandermonde::FeatureCache *fCache;

static Index **ind;
int *qidEvalSet;
int qCount;
int max_q_id = -1;

vector<string>* splitString(const std::string& str, char delim = ' ')
{
	vector<string> *res = new vector<string>;
    std::stringstream ss(str);
    std::string token;
    while (std::getline(ss, token, delim)) {
        res->push_back(token);
    }

    return res;
}

namespace LocalParameter {
int threadCount;
int windowSize;
// Smoothing Strategy: 0 for linear smoothing while 1 for dirichlet prior smoothing
int PLMSmoothMethod;

// Jelinek-Merce Smoothing parameter
double PLMJMLambda;

// Dirichlet Prior
double PLMDirPrior;

// Number of documents to be re-ranked
int RerankedDocCount;

// Propagation function (kernel)
int PropFunction;

// Standard variance for kernel density estimation, i.e., sigma
double PLMSigma;

// Score interpolation coefficient, i.e., weight of PLM, when interpolating plm based score with the original score
double PLMCoefficient;

// 0 : apply plm on single term query; 1 : do not apply plm on single term query
int IgnoreSingleTermQuery;

int doItweighted;

double coeff;

string qids;

void get() {
	PLMSigma = ParamGetDouble("PLMSigma", 175);

	PLMSmoothMethod = ParamGetInt("PLMSmoothMethod", 1);
	windowSize = ParamGetInt("OrderWindowSize", 5);
	PLMJMLambda = ParamGetDouble("PLMJMLambda", 0.5);
	PLMDirPrior = ParamGetDouble("PLMDirPrior", 500);

	RerankedDocCount = ParamGetInt("RerankedDocCount", 2000);

	// Default: Gaussian Kernel
	PropFunction = ParamGetInt("PropFunction", 0);

	PLMCoefficient = ParamGetDouble("PLMCoefficient", 1.0);


	IgnoreSingleTermQuery = ParamGetInt("IgnoreSingleTermQuery", 0);

	doItweighted = ParamGetInt("weightedPLM", 0);
	coeff = ParamGetDouble("coeff", 1.0);
	threadCount = ParamGetInt("threadCount", 4);
	qids = ParamGetString("queryIDs", "");

	if(qids.compare("") == 0){
		cerr<<"please specify queries in param file \n";
		exit(-1);
	}

	int range_boundaries[20];
	for(int i =0; i<20; i++)
		range_boundaries[i]= -1;


	int i =0;
	vector<string>* ranges = splitString(qids, ',');
	for(vector<string>::iterator it = ranges->begin(); it != ranges->end(); it++){
		vector<string> *range = splitString((*it), ':');
		int begin = atoi((*range)[0].c_str());
		int end = atoi((*range)[1].c_str());
		range_boundaries[i++] = begin;
		range_boundaries[i++] = end;
		qCount += (end - begin + 1);
		if(end>max_q_id)
			max_q_id = end;
		delete range;
	}
	delete ranges;

	qidEvalSet = new int[max_q_id+1];
	for(i=0; i<max_q_id+1; i++)
		qidEvalSet[i] = -1;

	for(i=0; i<20; i++){
		if(range_boundaries[i] == -1)
			break;

		int begin = range_boundaries[i++];
		int end = range_boundaries[i];

		for(int j=begin; j<end+1; j++ )
			qidEvalSet[j] = 1;
	}

}
}
;

void GetAppParam() {
	RetrievalParameter::get();

	SimpleKLParameter::get();

	LocalParameter::get();
}

//void PrintModel(const map<lemur::api::TERMID_T, lemur::api::SCORE_T>& myLM, Index& ind)
//{
//	map<lemur::api::TERMID_T, lemur::api::SCORE_T>::const_iterator mit;
//	for(mit = myLM.begin(); mit != myLM.end(); mit++)
//	{
//		cout << ind.term(mit->first) << "(" << mit->first << ")" << "\t" << mit->second << endl;
//	}
//}

/// Compute the "negative" KL-div of the query model and any document LM, where docLM has been smoothed
double KLDivergence(const map<lemur::api::TERMID_T, lemur::api::SCORE_T>& qLM,
		const map<lemur::api::TERMID_T, lemur::api::SCORE_T>& docLM) {
	double div = 0;
	map<lemur::api::TERMID_T, lemur::api::SCORE_T>::const_iterator mit;
	for (mit = qLM.begin(); mit != qLM.end(); mit++) {
		double pr = mit->second;
		int tid = mit->first;
		map<lemur::api::TERMID_T, lemur::api::SCORE_T>::const_iterator iter =
				docLM.find(tid);

		// docLM has been smoothed, so there is no zero prob.
		double doc_prob = iter->second;
		div += pr * log(pr / doc_prob);
	}

	return -div;
}

/// Smooth a PLM using linear smoothing method
/// i.e., LM = LM_mle * (1 - lambda) + LM_Col * lambda
void JMSmoothing(const map<lemur::api::TERMID_T, lemur::api::SCORE_T>& qLM,
		map<lemur::api::TERMID_T, lemur::api::SCORE_T>& docLM, Index& ind,
		const double lambda) {
	map<lemur::api::TERMID_T, lemur::api::SCORE_T>::const_iterator mit;
	for (mit = qLM.begin(); mit != qLM.end(); mit++) {
		int tid = mit->first;
		double col_prob = (double) (ind.termCount(tid))
				/ (double) (ind.termCount());

		map<lemur::api::TERMID_T, lemur::api::SCORE_T>::const_iterator iter =
				docLM.find(tid);
		double doc_prob =
				(iter == docLM.end()) ?
						lambda * col_prob :
						((1 - lambda) * iter->second + lambda * col_prob);
		docLM[tid] = doc_prob;
	}
}

/// Smooth a PLM using Dirichlet prior smoothing method
void DirSmoothing(const map<lemur::api::TERMID_T, lemur::api::SCORE_T>& qLM,
		map<lemur::api::TERMID_T, lemur::api::SCORE_T>& docLM, Index& ind,
		const double doc_len, const double mu) {
	double lambda = mu / (mu + doc_len);
	JMSmoothing(qLM, docLM, ind, lambda);
}

// Cumulative normal distribution function at x
double GaussianCDF(double x, double mean, double sigma) {
	double res;
	x = (x - mean) / sigma;
	if (x == 0) {
		res = 0.5;
	} else {
		double oor2pi = 1 / (sqrt(double(2) * pi));
		double t = 1 / (double(1) + 0.2316419 * fabs(x));
		t *=
				oor2pi * exp(-0.5 * x * x)
						* (0.31938153
								+ t
										* (-0.356563782
												+ t
														* (1.781477937
																+ t
																		* (-1.821255978
																				+ t
																						* 1.330274429))));
		if (x >= 0) {
			res = double(1) - t;
		} else {
			res = t;
		}
	}
	return res;
}

// Cumulative distribution function of Triangle Kernel
double TriangleCDF(double x, double mean, double sigma) {
	double res;
	x = (x - mean) / sigma;
	if (x >= 1) {
		res = sigma;
	} else if (x < -1) {
		res = 0;
	} else if (x < 0) {
		res = sigma * (1 - fabs(x)) * (1 - fabs(x)) / 2.0;
	} else {
		res = sigma - sigma * (1 - x) * (1 - x) / 2.0;
	}
	return res;
}

// Cumulative distribution function of Cosine Kernel
double CosineCDF(double x, double mean, double sigma) {
	double res;
	x = (x - mean) / sigma;
	if (x >= 1) {
		res = sigma;
	} else if (x < -1) {
		res = 0;
	} else if (x < 0) {
		res = sigma * (1 + x - sin(pi * x) / pi) / 2.0;
	} else {
		res = sigma - sigma * (1 - x + sin(pi * x) / pi) / 2.0;
	}

	//cout << res << endl;
	return res;
}

// Cumulative distribution function of Circle Kernel
double CircleCDF(double x, double mean, double sigma) {
	double res;
	x = (x - mean) / sigma;

	if (x >= 1) {
		res = (pi - 2.0) * sigma;
	} else if (x < -1) {
		res = 0;
	} else if (x < 0) {
		res = sigma * (asin(x) + pi / 2.0 - sqrt(1 - x * x));
	} else {
		res = (pi - 2.0) * sigma
				- sigma * (asin(-x) + pi / 2.0 - sqrt(1 - x * x));
	}
	return res;
}

// Cumulative distribution function of Arc Kernel
double ArcCDF(double x, double mean, double sigma) {
	double res;
	x = (x - mean) / sigma;

	if (x >= 1) {
		res = (pi - 1.0) * sigma / 2.0;
	} else if (x < -1) {
		res = 0;
	} else if (x < 0) {
		res = sigma
				* (asin(x) + pi / 2.0 - sqrt(1 - x * x)
						+ (1 - fabs(x)) * (1 - fabs(x)) / 2.0) / 2.0;
	} else {
		res = (pi - 1.0) * sigma / 2.0
				- sigma
						* (asin(-x) + pi / 2.0 - sqrt(1 - x * x)
								+ (1 - fabs(x)) * (1 - fabs(x)) / 2.0) / 2.0;
	}
	return res;
}

/// The propagated count from one position to another, when the distance between two positions is "dis"
/// Propagation function: -1 Passage; 0 Gaussian; 1: Cosine; 2: Triangle; 3: Arc; 4: Circle
/// Here, "dis" is normalized
double PropagationCount(double dis, int propFunction = 0) {
	dis = fabs(dis);
	switch (propFunction) {
	case -1:
		if (dis <= 1.0)
			return 1.0;
		else
			return 0;
	case 1:
		if (dis <= 1.0)
			return (1 + cos(pi * dis)) / 2.0;
		else
			return 0;
	case 2:
		if (dis <= 1.0)
			return 1 - dis;
		else
			return 0;
	case 3:
		if (dis <= 1.0)
			return (1 - dis + sqrt(1 - dis * dis)) / 2.0;
		else
			return 0;
	case 4:
		if (dis <= 1.0)
			return sqrt(1 - dis * dis);
		else
			return 0;
	case 0:
	default:
		return exp(-dis * dis / 2);
	}
}

/// The sum of propogated counts from all positions to the current position "pos", i.e., the size of the vitual passage
/// Propagation function: -1 Passage; 0 Gaussian; 1: Cosine; 2: Triangle; 3: Arc; 4: Circle
double PropagationCountSum(int pos, double doc_len, int propFunction = 0) {
	double sigma = LocalParameter::PLMSigma;
	double psg_len = 0;

	switch (propFunction) {
	case -1:
		if (sigma > doc_len - pos) {
			psg_len += doc_len - pos;
		} else {
			psg_len += sigma;
		}

		if (sigma > pos) {
			psg_len += pos;
		} else {
			psg_len += sigma;
		}
		break;

	case 1:
		psg_len = CosineCDF(doc_len, (double) pos, sigma)
				- CosineCDF(0, (double) pos, sigma);
		break;
	case 2:
		psg_len = TriangleCDF(doc_len, (double) pos, sigma)
				- TriangleCDF(0, (double) pos, sigma);
		break;
	case 3:
		psg_len = ArcCDF(doc_len, (double) pos, sigma)
				- ArcCDF(0, (double) pos, sigma);
		break;
	case 4:
		psg_len = CircleCDF(doc_len, (double) pos, sigma)
				- CircleCDF(0, (double) pos, sigma);
		break;
	case 0:
	default:
		psg_len = sqrt(double(2) * pi) * sigma
				* (GaussianCDF(doc_len, (double) pos, sigma)
						- GaussianCDF(0, (double) pos, sigma));
		break;
	}

	return psg_len;
}

class cacheItem {
public:
	TERMID_T first;
	TERMID_T second;
	double MI;
};

class MatchedTermInfo {
public:
	TERMID_T tid;
	int freq;
	const LOC_T* poses;
	const lemur::api::QueryTerm *qTerm;
};

vector<cacheItem> cache;

bool incache(int first, int sec, double &res) {
	for (int i = 0; i < cache.size(); i++)
		if (cache[i].first == first && cache[i].second == sec) {
			res = cache[i].MI;
			return true;
		}

	return false;
}

void addtocache(int first, int sec, double val) {
	cacheItem item;
	item.MI = val;
	item.first = first;
	item.second = sec;

	cache.push_back(item);
}

void intersectDocs(const LOC_T* firstPoses, int firstCount,
		const LOC_T* secPoses, int secCount, int* intersects) {

	int bestFirst = firstPoses[0];
	int bestSec = secPoses[0];
	int minDist = 9999999;

	bool direct(false), reverse(false);

	for (int i = 0; i < firstCount; i++)
		for (int j = 0; j < secCount; j++) {
			if (0 < firstPoses[i] - secPoses[j]
					&& firstPoses[i] - secPoses[j] < LocalParameter::windowSize)
				reverse = true;
			if (0 < secPoses[j] - firstPoses[i]
					&& secPoses[j] - firstPoses[i] < LocalParameter::windowSize)
				direct = true;

//			if (abs(firstPoses[i] - secPoses[j]) < minDist) {
//				minDist = abs(firstPoses[i] - secPoses[j]);
//				bestFirst = firstPoses[i];
//				bestSec = secPoses[j];
//			}
		}

	if (reverse)
		intersects[1]++;

	if (direct)
		intersects[0]++;

}

double computeMI(int totalTogether, int totalFirst, int totalSec, int total) {
	double res = 0;

	double pUnion = double(totalTogether) / double(total);
	double pFirst = double(totalFirst) / double(total);
	double pSec = double(totalSec) / double(total);
	double firstNotSec = double(totalFirst - totalTogether) / total;
	double secNotFirst = double(totalSec - totalTogether) / total;
	double pNone = double(total - totalFirst - totalSec + totalTogether)
			/ total;

	res += pUnion * log(pUnion / (pFirst * pSec));
	res += firstNotSec * log(firstNotSec / (pFirst * (1 - pSec)));
	res += secNotFirst * log(secNotFirst / (pSec * (1 - pFirst)));
	res += pNone * log(pNone / ((1 - pFirst) * (1 - pSec)));

	return res;
}

double ordered_MI(int first, int sec, Index *ind) {
	double res;

	if (incache(first, sec, res))
		return res;

	DocInfoList *dl1 = ind->docInfoList(first);
	DocInfoList *dl2 = ind->docInfoList(sec);

	int *intersects = new int[2];
	intersects[0] = intersects[1] = 0;
	int totalFirst = 0;
	int totalSec = 0;

	std::map<DOCID_T, MatchedTermInfo> mymap;
	DocInfo *d2Entry;
	dl2->startIteration();
	while (dl2->hasMore()) {
		d2Entry = dl2->nextEntry();
//		if (FeedbackDocs::isInFeedback(d2Entry->docID(), qid)) {
		totalSec++;
		MatchedTermInfo m;
		m.tid = d2Entry->docID();
		m.freq = d2Entry->termCount();
		m.poses = d2Entry->positions();
		mymap[m.tid] = m;
//		}
	}

// iterate over entries in docList
	dl1->startIteration();

	DocInfo *d1Entry;
	int totalTogether = 0;
	while (dl1->hasMore()) {
		d1Entry = dl1->nextEntry();
//		if (FeedbackDocs::isInFeedback(d2Entry->docID(), qid)) {
		totalFirst++;
		std::map<DOCID_T, MatchedTermInfo>::const_iterator got = mymap.find(
				d1Entry->docID());
		if (got != mymap.end()) {
			totalTogether++;
			MatchedTermInfo m = got->second;
			intersectDocs(d1Entry->positions(), d1Entry->termCount(), m.poses,
					m.freq, intersects);
		}
//		}
	}

	res = (double(intersects[0])) / (double(intersects[0] + intersects[1]));
	if (intersects[0] + intersects[1] == 0)
		res = 0.0;

//	res *= pow(2.0, computeMI(totalTogether, totalFirst, totalSec, ind->docCount()));
//	res *= computeMI(totalTogether, totalFirst, totalSec, ind->docCount());

	cout << "mi for: " << ind->term(first).data() << " and " << ind->term(sec)
			<< " is: " << res << endl;

	delete dl1;
	delete dl2;
	delete[] intersects;

	addtocache(first, sec, res);
	return res;
}

map<TERMID_T, POS_T> *getQTermPos(TextQuery *q, Index *ind) {
	map<TERMID_T, POS_T> *qTermPos = new map<TERMID_T, POS_T>();

	q->startTermIteration();
	int i = 0;
	while (q->hasMore()) {
		TERMID_T term = ind->term(q->nextTerm()->spelling());
		(*qTermPos)[term] = i++;
	}

	return qTermPos;
}

void updateWeights(map<TERMID_T, POS_T> *qTermsPos,
		vector<double*> &qTermPosListInDoc, Index *ind,
		map<string, double> &featureWeights) {

	for (int i = 0; i < qTermPosListInDoc.size(); i++) {
		qTermPosListInDoc[i][2] = featureWeights["UNIFORM"];

		qTermPosListInDoc[i][2] += fCache->getValue("UCF",
				ind->term(qTermPosListInDoc[i][0])) * featureWeights["UCF"];
		qTermPosListInDoc[i][2] += fCache->getValue("UWF",
						ind->term(qTermPosListInDoc[i][0])) * featureWeights["UWF"];

		for (int j = i + 1;
				j < qTermPosListInDoc.size()
						&& qTermPosListInDoc[j][1] - qTermPosListInDoc[i][1]
								< LocalParameter::windowSize; j++) {

			double *first = qTermPosListInDoc[i];
			double *sec = qTermPosListInDoc[j];

			if (abs((*qTermsPos)[sec[0]] - (*qTermsPos)[first[0]])
					< LocalParameter::windowSize &&
					abs((*qTermsPos)[sec[0]] - (*qTermsPos)[first[0]] > 0)) {

				string key = ind->term(qTermPosListInDoc[i][0]) + ","
						+ ind->term(qTermPosListInDoc[j][0]);

				qTermPosListInDoc[i][2] += fCache->getValue("PROX", key)
						* featureWeights["PROX"];
				qTermPosListInDoc[j][2] += fCache->getValue("PROX", key)
						* featureWeights["PROX"];

				qTermPosListInDoc[i][2] += fCache->getValue("WPRX", key)
						* featureWeights["WPRX"];
				qTermPosListInDoc[j][2] += fCache->getValue("WPRX", key)
						* featureWeights["WPRX"];
			}

			if (sec[1] - first[1] == 1
					&& (*qTermsPos)[sec[0]] - (*qTermsPos)[first[0]] == 1) {
				string key = ind->term(qTermPosListInDoc[i][0]) + ","
						+ ind->term(qTermPosListInDoc[j][0]);

				qTermPosListInDoc[i][2] += fCache->getValue("BCF", key)
						* featureWeights["BCF"];

				qTermPosListInDoc[i][2] += fCache->getValue("BWF", key)
										* featureWeights["BWF"];
			}

		}
	}
}

/// Compute the relevance of a document using PLM
double PLMDocScoring(const map<lemur::api::TERMID_T, lemur::api::SCORE_T>& qLM,
		const lemur::api::DOCID_T did, Index& ind,
		map<TERMID_T, POS_T>* qTermPoses, map<string, double> &featureWeights) {

	// sorted query terms by position ascending
	vector<double*> qTermPosList;

	const TermInfoList* tlist = ind.termInfoListSeq(did);
	int pos = 0;
	tlist->startIteration();
	while (tlist->hasMore()) {
		pos++;
		TermInfo* entry = tlist->nextEntry();
		lemur::api::TERMID_T tid = entry->termID();

		// if it is a query term
		if (qLM.find(tid) != qLM.end()) {
			double *item = new double[3];
			item[0] = tid;
			item[1] = pos;
			item[2] = 0.0;
			qTermPosList.push_back(item);
			//cout << ind.term(tid) << ":" << pos << "\n";
		}
	}
	delete tlist;

	if (LocalParameter::doItweighted == 1)
		updateWeights(qTermPoses, qTermPosList, &ind, featureWeights);

	const double doc_len = ind.docLength(did);

	// relevance score by PLM
	double plmScore = -1000000;

	// to improve efficiency, we only score positions where a query term occur.
	// intuitively, the best matching position would be at least close to these positions.
	// this is different from our previous implementation for sigir'09 paper, but the performance
	// is similar
	for (int i = 0; i < qTermPosList.size(); i++) {
		const int center = (int) qTermPosList[i][1];

		// the total propagated counts to the current position "center"
		// TODO ayyoub maybe we have to change this!!
		const double psg_len = PropagationCountSum(center, doc_len,
				LocalParameter::PropFunction);
		double new_psg_len = psg_len;

		// positional language model at "center"
		map<lemur::api::TERMID_T, lemur::api::SCORE_T> plm;

		for (int j = 0; j < qTermPosList.size(); j++) {
			const int pos = (int) qTermPosList[j][1];
			const int tid = qTermPosList[j][0];

			// the (normalized) propagated count from a query term "tid" at position "pos"
//			double pr = PropagationCount((double) (pos - center) / LocalParameter::PLMSigma, LocalParameter::PropFunction) / psg_len;
			double pr = PropagationCount(
					(double) (pos - center) / LocalParameter::PLMSigma,
					LocalParameter::PropFunction) / psg_len;

			if (LocalParameter::doItweighted == 1) {
				new_psg_len += pr * (qTermPosList[j][2] - 1.0);
//				cout<<"adding: "<<pr*(qTermPosList[j][2] - 1.0)<< "to: " << psg_len<<"getting: "<<new_psg_len<<endl;
				pr *= qTermPosList[j][2];
			}

			if (pr > 0) {
				if (plm.find(tid) == plm.end()) {
					plm.insert(make_pair(tid, pr));
				} else {
					plm[tid] += pr;
				}
			}
		}

		if (LocalParameter::doItweighted == 1) {
			double coeff = psg_len / new_psg_len;
			for (int j = 0; j < qTermPosList.size(); j++) {
				const int tid = qTermPosList[j][0];

				plm[tid] *= coeff;
			}
		}

		// smooth PLM
		if (LocalParameter::PLMSmoothMethod == 0) {
			JMSmoothing(qLM, plm, ind, LocalParameter::PLMJMLambda);
		} else // if(LocalParameter::PLMSmoothingStrategy == 1)
		{
			DirSmoothing(qLM, plm, ind, psg_len, LocalParameter::PLMDirPrior);
		}

		// KL-divergence
		double score = KLDivergence(qLM, plm);

		if (score > plmScore) {
			plmScore = score;
		}
	}

	for (int i = 0; i < qTermPosList.size(); i++)
		delete qTermPosList[i];
	return plmScore;
}

/// Rerank result documents using PLM 
IndexedRealVector* PLMReranking(
		const map<lemur::api::TERMID_T, lemur::api::SCORE_T>& qLM,
		IndexedRealVector& retResults, Index& ind,
		map<TERMID_T, POS_T>* qTermPoses, map<string, double> &featureWeights) {

	IndexedRealVector *sortedList = new IndexedRealVector();
	IndexedRealVector::iterator dit;
	for (dit = retResults.begin(); dit != retResults.end(); dit++) {
		lemur::api::DOCID_T did = dit->ind;
		double score = dit->val * (1 - LocalParameter::PLMCoefficient)
				+ PLMDocScoring(qLM, did, ind, qTermPoses, featureWeights)
						* LocalParameter::PLMCoefficient;
		sortedList->PushValue(did, score);

		if (sortedList->size() >= LocalParameter::RerankedDocCount) {
			break;
		}
	}

	sortedList->Sort();

	return sortedList;
}

void execProgram(string name) {
	int stat = system(name.c_str());
}

double computeMap(string filename, int &qCount, int retry=0) {
    string prog_name = string(
            "bash -c \"../trec_eval/trec_eval -q ../data/judgments/jud-ap.txt ") + filename
                       + string(" 2>errors|grep \"^map\" | tee >(grep all > ") + filename
                       + string(".map) | grep -v all | wc -l >") + filename + string(".qCount\"");

    execProgram(prog_name);

    ifstream infile((filename + string(".map")).c_str());
    ifstream infile2((filename + string(".qCount")).c_str());
    if (infile.is_open() && infile2.is_open()) {

        string line;
        infile >> line >> line >> line;
        string line2;
        infile2>>line2;

        if (line.length() > 0 && line2.length() > 0) {
            qCount = atoi(line2.c_str());
            return atof(line.c_str());
        } else {
            if(retry == 0){
                sleep(1);
                return computeMap(filename, qCount, retry+1);
            }
            cerr << "couldn't calculate map for given file: "<<(filename + string(".map")) << endl;
            exit(0);
        }
    } else {
        cerr << "no such result map file: "<< (filename + string(".map")) << endl;
        exit(0);
    }
}

struct doItUsingThreadStruct
{
	map<string, double> *featureWeights;
	map<int, IndexedRealVector *> *m;
	map<int, map<lemur::api::TERMID_T, lemur::api::SCORE_T> *> * qLMs;
	map<int, map<TERMID_T, POS_T>*> *qTermsPoses;
	int batchNom;
	int interval;
};

void *doItUsingThread(void * arg) {
	doItUsingThreadStruct *args = (doItUsingThreadStruct *) arg;


	ofstream result((RetrievalParameter::resultFile+to_string(args->batchNom)).c_str());
	ResultFile resFile(RetrievalParameter::TRECresultFileFormat);
	resFile.openForWrite(result, *ind[args->batchNom]);

	if (!result.is_open()) {
		cerr << "couldn't write results to file";
		exit(-1);
	}

	map<lemur::api::TERMID_T, lemur::api::SCORE_T> *qLM;
	IndexedRealVector * wplmRes;
	int beg = args->batchNom * args->interval;
	int end = (args->batchNom + 1) * args->interval;
	int i =0;
	for (int j=0 ; j<max_q_id+1; j++) {
		if(i>= beg && i<end and qidEvalSet[j] == 1){
//			cout<<"train query nom: "<<j<<endl;
//			cout<<"thread "<<args->batchNom<<" q "<<it.first<<endl;
			IndexedRealVector * results = (*(args->m))[j];
			qLM = (*(args->qLMs))[j];

			// do we apply PLM on single-term queries?
			if (LocalParameter::IgnoreSingleTermQuery == 1 && qLM->size() == 1) {
				resFile.writeResults(to_string(j), wplmRes,
						RetrievalParameter::resultCount);
			} else {
				wplmRes = PLMReranking(*qLM, *results, *ind[args->batchNom],
						(*(args->qTermsPoses))[j], *(args->featureWeights));
				resFile.writeResults(to_string(j), wplmRes,
						RetrievalParameter::resultCount);
				delete wplmRes;
			}
		}

		if(qidEvalSet[j] == 1)
			i++;

	}
	result.close();
//	cout<<"thread "<<args->batchNom<<" ended";
	pthread_exit(NULL);
}

double getWeightedPLMMap(map<string, double> &featureWeights, Index **ind, int& queryCount) {

	static map<int, IndexedRealVector *> *m;
	static map<int, map<lemur::api::TERMID_T, lemur::api::SCORE_T> *> * qLMs;
	static map<int, map<TERMID_T, POS_T>*> *qTermsPoses;

	IndexedRealVector *results;
	TextQuery *q;
	map<lemur::api::TERMID_T, lemur::api::SCORE_T> *qLM;

	if (m == NULL) {
		std::filebuf wofb;
		wofb.open("results/kl.txt", std::ios::out);
		std::ostream woos(&wofb);
		ResultFile woResFile;

		woResFile.openForWrite(woos, *ind[0]);
		DocStream *qryStream;
		try {
			qryStream = new lemur::parse::BasicDocStream(
					RetrievalParameter::textQuerySet);
		} catch (Exception &ex) {
			ex.writeMessage(cerr);
			throw Exception("RetEval",
					"Can't open query file, check parameter textQuery");
		}

		lemur::retrieval::ArrayAccumulator accumulator(ind[0]->docCount());
		RetrievalMethod *model = NULL;
		model = RetMethodManager::createModel(ind[0], &accumulator,
				RetrievalParameter::retModel);

		qryStream->startDocIteration();

		m = new map<int, IndexedRealVector *>;
		qLMs = new map<int, map<lemur::api::TERMID_T, lemur::api::SCORE_T> *>;
		qTermsPoses = new map<int, map<TERMID_T, POS_T>*>();

		while (qryStream->hasMore()) {

			results = new IndexedRealVector(ind[0]->docCount());
			Document *d = qryStream->nextDoc();
			q = new TextQuery(*d);
//			cout << "query : "<< q->id() << endl;

			QueryRep * qr = model->computeQueryRep(*q);

			lemur::retrieval::SimpleKLQueryModel * klqr =
					(lemur::retrieval::SimpleKLQueryModel *) qr;
			qLM = new map<lemur::api::TERMID_T, lemur::api::SCORE_T>;
			klqr->startIteration();
			while (klqr->hasMore()) {
				QueryTerm *qt;
				qt = klqr->nextTerm();
				lemur::api::TERMID_T tid = qt->id();
				qLM->insert(make_pair(tid, qt->weight() / klqr->totalCount()));
				delete qt;
			}

			model->scoreCollection(*qr, *results);
			results->Sort();
			woResFile.writeResults(q->id(), results, 1000);

			(*m)[atoi(q->id().c_str())] = results;
			(*qLMs)[atoi(q->id().c_str())] = qLM;
			(*qTermsPoses)[atoi(q->id().c_str())] = getQTermPos(q, ind[0]);
		}

		delete model;
		wofb.close();
	}

	vector<int> allQids;
	for (auto const &it : *m) {
		if(qidEvalSet[it.first] == 1)
			allQids.push_back(it.first);
	}
	int interval = allQids.size() / LocalParameter::threadCount + 1;

	int rc;
	pthread_t threads[LocalParameter::threadCount];
	doItUsingThreadStruct args[LocalParameter::threadCount];
	for (int i = 0; i < LocalParameter::threadCount; i++){
		args[i].batchNom = i;
		args[i].featureWeights = &featureWeights;
		args[i].interval = interval;
		args[i].m = m;
		args[i].qLMs = qLMs;
		args[i].qTermsPoses = qTermsPoses;
		rc = pthread_create(&threads[i], NULL, doItUsingThread, (void*)&args[i]);

	     if (rc) {
	         cout << "Error:unable to create thread," << rc << endl;
	         exit(-1);
	      }
	}

	double res = 0.0;
	void * status;
    int totComputedQCount =0;
    int computedQCount;
    double map;
	for(int i =0; i< LocalParameter::threadCount; i++){
		rc = pthread_join(threads[i], &status);
		if (rc) {
		 cout << "Error:unable to join," << rc << endl;
		 exit(-1);
		}

        map = computeMap(RetrievalParameter::resultFile+to_string(i), computedQCount) ;

        totComputedQCount += computedQCount;
        res += map * computedQCount;
    }
    return res/totComputedQCount;
}

double optimLibWrapper(const arma::vec& vals_inp, arma::vec* grad_out,
		void* opt_data) {

	map<string, double> features;
	features["UCF"] = vals_inp[0];
	features["BCF"] = vals_inp[1];
	features["PROX"] = vals_inp[2];
	features["UNIFORM"] = vals_inp[3];
	features["UWF"] = vals_inp[4];
	features["BWF"] = vals_inp[5];
	features["WPRX"] = vals_inp[6];

	int dummy_query_count;
	double res = getWeightedPLMMap(features, ind, dummy_query_count);

    const double widthSize = 8;
    cout<<std::left;
	cout << "UCF: " << setw(widthSize) << features["UCF"]
         << "BCF: " << setw(widthSize) << features["BCF"]
         << "PROX: "<< setw(widthSize) << features["PROX"]
         << "UNIFORM: " << setw(widthSize) << features["UNIFORM"]
         << "UWF: " << setw(widthSize) << features["UWF"]
         << "BWF: " << setw(widthSize) << features["BWF"]
         << "WPROX: " << setw(widthSize) << features["WPROX"]
         << "res: " << setw(widthSize) << res << endl;

	return -res;
}

bool updateInitialWeights(arma::vec &x_init, arma::vec &x) {
	int i = 0;
	for (; i <= 7; i++)
		if (i == 7)
			break;
		else if (0.9 - x_init[i] > 0.05)
			break;

	if (i == 7)
		return false;

	for (int j = 0; j < i; j++)
		x_init[j] = 0.0;

//	cout<<"increasing: x["<<i<<"]: from "<<x_init[i]<<endl;
//	cout<<"means: "<<x_init[i]<< " + "<< 0.1 <<" = " <<x_init[i] + 0.1<<endl;
	x_init[i] += 0.1;

//	cout<<"now it is: " <<x_init<<endl;
	for (int i = 0; i < 7; i++)
		x[i] = x_init[i];

	return true;
}


void splitTestTrain(int mode){
	int halfCount = qCount/2;
	int inFirsthal=0;
	for(int i =0; i< max_q_id+1; i++)
		if(mode == 0)
			if(inFirsthal <= halfCount){
				if(qidEvalSet[i] == 0 or qidEvalSet[i] == 1){
					qidEvalSet[i] = 1;
					inFirsthal ++;
				}
			}else{
				if(qidEvalSet[i] == 0 or qidEvalSet[i] == 1){
					qidEvalSet[i] = 0;
					inFirsthal ++;
				}
			}
		else
			if(inFirsthal <= halfCount){
				if(qidEvalSet[i] == 0 or qidEvalSet[i] == 1){
					qidEvalSet[i] = 0;
					inFirsthal ++;
				}
			}else{
				if(qidEvalSet[i] == 0 or qidEvalSet[i] == 1){
					qidEvalSet[i] = 1;
					inFirsthal ++;
				}
			}
}

void swapTrainTestSet(){
	for(int i =0; i< max_q_id+1; i++)
		if(qidEvalSet[i] == 0)
			qidEvalSet[i] = 1;
		else if(qidEvalSet[i] == 1)
			qidEvalSet[i] = 0;
}


//MAPFunction
#define N_FEATURES 7

class MAPFunction : public MultivariateRealFunction {
public:
    MAPFunction(lemur::api::Index** index);
    virtual double abstract_eval(std::vector<double> x);
    int get_query_count();
private:
    lemur::api::Index ** index;
};

std::map<string, double> feature_weights_vector_to_map(std::vector<double> weights);
std::vector<double> feature_weights_vector_to_map(std::map<string, double> weights);

MAPFunction::MAPFunction(Index** index) {
    this->index = index;
    n_inputs = N_FEATURES;
    for (int i = 0; i < n_inputs; i++)
        domain.push_back(Interval(0,1));

    // to ensure that the unigram weight is always kept relatively high
    // domain[0] = Interval(0.6,1);
    // domain[4] = Interval(0.4,1);

    consistency_check();
}

double MAPFunction::abstract_eval(std::vector<double> x) {
    map<string, double> features = feature_weights_vector_to_map(x);
    int n_valid_queries;
    return -getWeightedPLMMap(features, index, n_valid_queries);
}

int MAPFunction::get_query_count() {
	int result;

	std::vector<double> x(N_FEATURES, 1);
    map<string, double> features = feature_weights_vector_to_map(x);
	getWeightedPLMMap(features, index, result);

	return result;
}


//two utility functions
map<string, double> feature_weights_vector_to_map(vector<double> weights) {
    if (weights.size() != N_FEATURES)
        throw domain_error("weights_vector given to feature_weights_vector_to_map must have a size of 7");

    map<string, double> result;
    result["UCF"] = weights[0];
    result["BCF"] = weights[1];
    result["PROXCF"] = weights[2];
    result["UNIFORM"] = weights[3];
    result["UDF"] = weights[4];
    result["BDF"] = weights[5];
    result["PROXDF"] = weights[6];

    return result;
};

vector<double> feature_weights_vector_to_map(map<string, double> weights) {
    vector<double> result(N_FEATURES);
    result[0] = weights["UCF"];
    result[1] = weights["BCF"];
    result[2] = weights["PROXCF"];
    result[3] = weights["UNIFORM"];
    result[4] = weights["UDF"];
    result[5] = weights["BDF"];
    result[6] = weights["PROXDF"];
    return result;
}

#define N_FOLDS 2


/// A retrieval evaluation program
int AppMain(int argc, char *argv[]) {

    if (argc < 3) {
        cerr << "call like: PLMRetEval paramfile cachefile";
        exit(-1);
    }
    char * cachefile = argv[2];

    ind = new Index*[LocalParameter::threadCount];
    try {
        for (int i = 0; i < LocalParameter::threadCount; i++)
            ind[i] = IndexManager::openIndex(RetrievalParameter::databaseIndex);
    } catch (Exception &ex) {
        ex.writeMessage();
        throw Exception("RelEval", "Can't open index, check parameter index");
    }

    fCache = new vandermonde::FeatureCache(cachefile, ind[0],
                                           LocalParameter::windowSize);


   splitTestTrain(0);

   MAPFunction map_function(ind);
   CoarseFineOptimizer optimizer(&map_function);
   std::vector<double> test_MAPs;
   std::vector<int> test_MAP_weights;

   for (int j = 0; j < 3; j++) {

	   for (int i = 0; i < N_FOLDS; i++) {
		   Solution optimized_solution = optimizer.optimize_multiple_times(6, 8, 8); //last value: 10, 8, 12
		   swapTrainTestSet();

		   double test_MAP = map_function.eval(optimized_solution.get_x());
		   test_MAPs.push_back(test_MAP);

		   cout<<"test MAP is: "<<test_MAP<<endl;

		   test_MAP_weights.push_back(map_function.get_query_count());
		}

		cout<<endl<<"final average is: "<<calculate_weighted_average(test_MAPs, test_MAP_weights)<<endl;

		cout<<" ========================= end of a complete round of optimization ===================="<<endl;
   }




    // =================== NELDER MEAD SECTION ======================
    
    /*arma::vec x_init = arma::zeros(7, 1);
    x_init[0] = -0.1;

    optim::algo_settings_t settings;
    settings.vals_bound = true;
    settings.lower_bounds = arma::zeros(7, 1);
    settings.upper_bounds = arma::ones(7, 1);
    settings.err_tol = 0.001;
    settings.iter_max = 100;

    arma::vec x = x_init;
    for (int i = 0; i < 7; i++) {
        x[i] = 0.5;
    }
    cout<<"before first nm call"<<endl;
    bool success = optim::nm(x, optimLibWrapper, nullptr, settings);
	cout<<"after first nm call"<<endl;

    while (updateInitialWeights(x_init, x)) {

        std::chrono::time_point<std::chrono::system_clock> start =
                std::chrono::system_clock::now();

        bool success = optim::nm(x, optimLibWrapper, nullptr, settings);
		cout<<"after loop nm call"<<endl;

        std::chrono::time_point<std::chrono::system_clock> end =
                std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end - start;

        if (success) {
            std::cout << "de: Ackley test completed successfully.\n"
                      << "elapsed time: " << elapsed_seconds.count() << "s\n";

            arma::cout << "\nde: solution to Ackley test:\n" << x << arma::endl;
            swapTrainTestSet();
            cout<<"testing results: "<<endl;
            optimLibWrapper(x, NULL, NULL);
            swapTrainTestSet();
        } else {
            std::cout << "de: Ackley test completed unsuccessfully."
                      << std::endl;
        }


//		fCache->saveCache(cachefile);
    }*/
	



//    swapTrainTestSet();
//    map<string, double> optimized_solution_map(feature_weights_vector_to_map(optimized_solution.getX()));
//    getWeightedPLMMap(optimized_solution_map, ind);

//    fCache->saveCache(cachefile);

    delete ind;
    return 0;
}
