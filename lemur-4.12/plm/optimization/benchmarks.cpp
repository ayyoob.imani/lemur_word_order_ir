#include <cmath>
#include "benchmarks.h"

//sphere function

double benchmarks::SphereFunction::abstract_eval(std::vector<double> x) {
    double result = 0;
    for (int i = 0; i < n_inputs; i++) {
        result += pow(x[i], 2);
    }
    return result;
}

benchmarks::SphereFunction::SphereFunction(int _n_inputs) {
    n_inputs = _n_inputs;
    for (int i = 0; i < n_inputs; i++) {
        domain.push_back(Interval(-10,10));
    }
    consistency_check();
}


//Ackley function

benchmarks::AckleyFunction::AckleyFunction() {
    n_inputs = 2;
    domain.push_back(Interval(-50,50));
    domain.push_back(Interval(-50,50));
    consistency_check();
}


double benchmarks::AckleyFunction::abstract_eval(std::vector<double> x) {
    double p1 = -20 * exp(-0.2 * sqrt(0.5 * (pow(x[0],2) + pow(x[1],2))));
    double p2 = exp(0.5 * (cos(2*M_PI*x[0]) + cos(2*M_PI*x[1])));

    return p1 - p2 + exp(1) + 20;
}

