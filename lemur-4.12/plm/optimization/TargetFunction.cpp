#include <cmath>
#include "TargetFunction.h"

TargetFunction::TargetFunction() {
    n_inputs = 3;
    domain.push_back(Interval(-10,10));
    domain.push_back(Interval(-20,10));
    domain.push_back(Interval(-50,20));
    consistency_check();
}

double TargetFunction::abstract_eval(std::vector<double> x) {
    return pow(x[0] - 3, 2) + pow(x[1], 2) - pow(x[2], 4);
}

