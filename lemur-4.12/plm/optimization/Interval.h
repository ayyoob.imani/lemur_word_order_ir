#ifndef OPTIMIZATION_INTERVAL_H
#define OPTIMIZATION_INTERVAL_H

#include <string>
#include <sstream>

struct Interval {
public:
    Interval(double start, double end) {
        this->start = start;
        this->end = end;
    }
    double get_length() const{
        return end - start;
    }
    std::string to_string() const;
    double start, end;
};


#endif
