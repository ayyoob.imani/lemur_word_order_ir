#include <iostream>
#include <vector>
#include "utility.h"
#include "SAOptimizer.h"
#include "TargetFunction.h"
#include "benchmarks.h"
#include "CoarseFineOptimizer.h"

using namespace std;

//TODO: don't just change one dimension at each iteration. change a random number of them.

int main() {
    benchmarks::AckleyFunction f;
    CoarseFineOptimizer opt(&f);
    opt.optimize_multiple_times(200);
}