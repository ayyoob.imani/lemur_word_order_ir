#include "Solution.h"

#include "utility.h"
#include <string>

using namespace std;

// Solution class
Solution::Solution(MultivariateRealFunction* _target_function) : target_function(_target_function) {
    x.resize(target_function->get_n_inputs());
    energy_is_calculated = false;
    energy = 0;
}

Solution::Solution(MultivariateRealFunction* _target_function, std::vector<double> x) :
        Solution(_target_function) {
    this->x = x;
}

Solution::Solution(MultivariateRealFunction* _target_function, int length) :
        Solution(_target_function) {
    this->x.resize(length);
}

Solution Solution::generate_random(MultivariateRealFunction* target_function) {
    int solution_length = target_function->get_n_inputs();
    Solution result(target_function, solution_length);
    for (int i = 0; i < solution_length; i++) {
        Interval variable_domain = target_function->get_domain()[i];
        result[i] = random_double(variable_domain.start, variable_domain.end);
    }
    return result;
}

/*
Solution Solution::get_random_neighbor(double step_size) {
    Solution result(target_function, x);

    int random_input_index = random_int(0, x.size() - 1);
    double d = random_double(0,1);
    double change_value =  d < 0.5 ? step_size : - step_size;
    result[random_input_index] += change_value;

    double upper_limit = target_function->get_domain()[random_input_index].end;
    double lower_limit = target_function->get_domain()[random_input_index].start;

    if (result[random_input_index] > upper_limit) {
        result[random_input_index] = upper_limit;

    } else if (result[random_input_index] < lower_limit) {
        result[random_input_index] = lower_limit;
    }

    return result;
}*/


Solution Solution::get_random_neighbor(double step_size) {
    Solution result(target_function, x);

    for (int i = 0; i < result.size(); i++) {
        double random = random_double(0,1);

        double change_value;
        if (random < 0.33)
            change_value = step_size;
        else if (random < 0.66)
            change_value = - step_size;
        else
            change_value = 0;

        result[i] += change_value;

        double upper_limit = target_function->get_domain()[i].end;
        double lower_limit = target_function->get_domain()[i].start;

        if (result[i] > upper_limit) {
            result[i] = upper_limit;

        } else if (result[i] < lower_limit) {
            result[i] = lower_limit;
        }
    }

    return result;
}

double Solution::calculate_energy() {
    energy =  target_function->eval(x);
    energy_is_calculated = true;
    return energy;
}


std::string Solution::to_string() {
    return string("(") + join(x, ",") + ")";
}

Solution::Solution(const Solution &other) : target_function(other.target_function) {
    x = other.x;
}

Solution &Solution::operator=(Solution &other) {
    this->target_function = other.target_function;
    this->x = other.x;
    this->energy_is_calculated;
    this->energy;
}

double Solution::get_energy() {
    if (energy_is_calculated)
        return energy;
    else
        calculate_energy();
}


