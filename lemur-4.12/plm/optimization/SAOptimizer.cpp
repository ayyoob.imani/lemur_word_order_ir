#include "SAOptimizer.h"
#include "utility.h"

#include <cstdlib>
#include <ctime>
#include <cmath>
#include <iostream>
#include <cstdio>

using namespace std;

//SAOptimizer class

SAOptimizer::SAOptimizer(MultivariateRealFunction* _target_function) :
        target_function(_target_function),
        temperature(20.0),
        cooling_rate(0.1),
        step_size(0.1),
        energy_change_inactivity_threshold(0.003),
        n_inactive_iterations_threshold(60),
        temperature_coefficient(0.0001) {
    srand(time(NULL)); //used for creating random solutions
}

Solution SAOptimizer::optimize() {
    cout<<"\n\n=====================OPTIMIZATION BEGAN========================\n\n";
    int n_inactive_iterations = 0;

//    vector<double> baseline = {0,0,0,1,0,0,0};
//    Solution current_solution = Solution(target_function,baseline),
    Solution current_solution = Solution::generate_random(target_function),
             initial_solution = current_solution,
             best_solution = current_solution;

    double current_energy = current_solution.calculate_energy(),
           initial_energy = current_energy,
           best_energy = current_energy;

    cout<<"initial solution "<<initial_solution.to_string()<<" "<<current_energy<<endl<<endl;

    while (temperature > 0.005) {
        Solution neighbor = current_solution.get_random_neighbor(step_size);
        double neighbor_energy = neighbor.calculate_energy();
        cout << "new neighbor: " <<endl;
        cout<< "difference: (" << join(vector_difference(neighbor.get_x(), current_solution.get_x()), ",") << ") "<< neighbor_energy - current_energy <<endl;
        cout<< "absolute: " << neighbor.to_string() <<" "<< neighbor_energy<<endl;

        bool energy_change_too_small, neighbor_accepted;

        double acceptance_prob = acceptance_probability(current_energy, neighbor_energy);
        neighbor_accepted = acceptance_prob > random_double(0, 1);
        cout << "acceptance probabilty: (" << current_energy << ", " << neighbor_energy << ", " << temperature <<") -> "<<acceptance_prob<< endl;
        if (neighbor_accepted) {
            energy_change_too_small = abs(current_energy - neighbor_energy) < energy_change_inactivity_threshold;

            current_solution = neighbor;
            current_energy = neighbor_energy;

            if (current_energy < best_energy) {
                cout<<"new best solution: "<<best_solution.to_string()<<" "<<best_energy<<endl<<endl;
                best_energy = current_energy;
                best_solution = current_solution;
            }
        }

        cout << "neighbor accepted: " << neighbor_accepted << endl;
        if (neighbor_accepted)
            cout << "energy change too small: " << energy_change_too_small << endl;

        if (neighbor_accepted && energy_change_too_small) {
            cout<<"detected inactivity. n_inanctive_iterations: "<<n_inactive_iterations<<endl;
            n_inactive_iterations++;
        } else {
            n_inactive_iterations = 0;
        }

        if (n_inactive_iterations > n_inactive_iterations_threshold) {
            cout<<"too many inactive iterations. stopping optimization"<<endl;
            break;
        }

        cout<<endl;
        update_temperature();
    }
    cout<<endl<<endl;
    cout<<"initial solution: "<<initial_solution.to_string()<<" "<<initial_energy<<endl;
    cout<<"best solution: "<<best_solution.to_string()<<" "<<best_energy<<endl<<endl;
    return best_solution;
}

void SAOptimizer::update_temperature() {
    temperature *= 1 - cooling_rate;
}

double SAOptimizer::acceptance_probability(double current_energy, double new_energy) {
    if (new_energy < current_energy)
        return 1.0;

    double p = (current_energy - new_energy) / (temperature_coefficient * temperature);
    double r = exp(p);
    return r;
}



