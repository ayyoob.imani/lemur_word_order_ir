#ifndef OPTIMIZATION_BENCHMARKS_H
#define OPTIMIZATION_BENCHMARKS_H

#include "MultivariateRealFunction.h"

namespace benchmarks {

    class SphereFunction : public MultivariateRealFunction {
    public:
        SphereFunction(int n_inputs);
        virtual double abstract_eval(std::vector<double> x);
    };

    class AckleyFunction : public MultivariateRealFunction {
    public:
        AckleyFunction();
        virtual double abstract_eval(std::vector<double> x);
    };

};

#endif //OPTIMIZATION_BENCHMARKS_H
