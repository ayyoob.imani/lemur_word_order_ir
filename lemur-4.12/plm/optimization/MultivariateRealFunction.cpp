#include "MultivariateRealFunction.h"
#include <stdexcept>

using namespace std;

void MultivariateRealFunction::consistency_check() {
    if (n_inputs != domain.size())
        throw domain_error("consistency error in MultivariateRealFunction: n_inputs does not equal size of domain vector.");
}

int MultivariateRealFunction::get_evaluated_n_times() const {
    return evaluated_n_times;
}

MultivariateRealFunction::MultivariateRealFunction() {
    evaluated_n_times = 0;
}

double MultivariateRealFunction::eval(std::vector<double> x) {
    evaluated_n_times++;
    return abstract_eval(x);
}
