#ifndef OPTIMIZATION_UTILITY_H
#define OPTIMIZATION_UTILITY_H

#include <cstdlib>
#include <string>
#include <vector>
#include <sstream>

double random_double(double rand_min, double rand_max);
int random_int(int rand_min, int rand_max);

template <typename T>
std::string join(std::vector<T> arr, std::string delimiter) {
    std::stringstream ss;
    for (int i = 0; i < arr.size() - 1; i++) {
        ss << arr[i] << delimiter;
    }
    ss << arr[arr.size() - 1];
    return ss.str();
}


template<typename T>
std::vector<T> vector_difference(std::vector<T> a, std::vector<T> b) {
    std::vector<T> result(a.size());
    for (int i = 0; i <result.size(); i++) {
        result[i] = a[i] - b[i];
    }
    return result;
}

#endif
