#ifndef OPTIMIZATION_COARSEFINEOPTIMIZER_H
#define OPTIMIZATION_COARSEFINEOPTIMIZER_H

#include "MultivariateRealFunction.h"
#include "Interval.h"
#include "Solution.h"

#include <utility>
#include <vector>

class CoarseFineOptimizer {
public:
    CoarseFineOptimizer(MultivariateRealFunction* target_function);
    Solution optimize(int n_stages, int n_solutions_per_stage);
    Solution optimize_multiple_times(int n_times, int n_stages, int n_solutions_per_stage);
//    Solution optimize(std::string output_file_name);

private:
//    Solution optimize(std::ostream& out_stream);

    MultivariateRealFunction* target_function;
    Solution generate_random_constrained_solution(std::vector<Interval> constraint);

    std::pair<Solution, double> run_optimization_stage(std::vector<Interval> search_domain, int n_solutions);
    void focus_search_domain_around_solution(Solution& focus_point, std::vector<Interval>& search_domain);

};

#endif
