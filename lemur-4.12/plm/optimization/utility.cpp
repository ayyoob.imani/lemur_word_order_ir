#include "utility.h"
#include <iostream>
#include <cstdio>

using namespace std;

double random_double(double rand_min, double rand_max) {
    double r = (double) rand() / RAND_MAX;
//    printf("at random_double, r: %f, rand_min: %f, rand_max: %f", r, rand_min, rand_max);
    return rand_min + r * (rand_max - rand_min);
}


int random_int(int rand_min, int rand_max) {
    return (rand() % (rand_max - rand_min + 1)) + rand_min;
}
