#ifndef OPTIMIZATION_MULTIVARIATEREALFUNCTION_H
#define OPTIMIZATION_MULTIVARIATEREALFUNCTION_H

#include <vector>
#include "Interval.h"

class MultivariateRealFunction {
public:
    MultivariateRealFunction();
    double eval(std::vector<double> x);
    virtual double abstract_eval(std::vector<double> x) = 0;
    int get_n_inputs() const { return n_inputs; }
    std::vector<Interval> get_domain() const { return  domain; }
    int get_evaluated_n_times() const;
protected:
    int n_inputs;
    std::vector<Interval> domain;
    void consistency_check();
    int evaluated_n_times;
};



#endif
