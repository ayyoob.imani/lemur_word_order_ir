#ifndef OPTIMIZATION_TARGETFUNCTION_H
#define OPTIMIZATION_TARGETFUNCTION_H

#include "MultivariateRealFunction.h"

class TargetFunction : public MultivariateRealFunction {
    int evaluated_n_times;
public:
    TargetFunction();
    virtual double abstract_eval(std::vector<double> x);
};

#endif
