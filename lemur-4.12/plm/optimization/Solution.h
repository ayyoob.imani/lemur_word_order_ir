#ifndef OPTIMIZATION_SOLUTION_H
#define OPTIMIZATION_SOLUTION_H

#include "MultivariateRealFunction.h"

#include <vector>
#include <string>

class Solution {
public:
    Solution(MultivariateRealFunction* target_function);
    Solution(MultivariateRealFunction* target_function, std::vector<double> x);
    Solution(MultivariateRealFunction* target_function, int length);

    Solution(const Solution& other);
    Solution& operator=(Solution& other);

    static Solution generate_random(MultivariateRealFunction* target_function);
    Solution get_random_neighbor(double step_size);
    double& operator[](int index) { return x[index]; }

    double calculate_energy();
    double get_energy();

    std::string to_string();
    unsigned long size() const { return x.size(); }
    std::vector<double> get_x() const { return x; }

//    double get_x_at(int index) { return x[index]; }
//    double set_x_at(int index, double value) { x[index] = value; }
private:
    MultivariateRealFunction* target_function;
    std::vector<double> x;

    bool energy_is_calculated;
    double energy;
};



#endif //OPTIMIZATION_SOLUTION_H
