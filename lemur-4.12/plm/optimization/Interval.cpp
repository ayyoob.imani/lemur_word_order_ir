#include "Interval.h"

using namespace std;

std::string Interval::to_string() const {
    stringstream ss;
    ss << "(" << start << ", " << end << ")";
    return ss.str();
}
