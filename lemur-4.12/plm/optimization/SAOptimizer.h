#ifndef OPTIMIZATION_SAOPTIMIZER_H
#define OPTIMIZATION_SAOPTIMIZER_H

#include "MultivariateRealFunction.h"
#include "Solution.h"

#include <string>

class SAOptimizer {
public:
    explicit SAOptimizer(MultivariateRealFunction* _target_function);
    Solution optimize();
private:
    MultivariateRealFunction* target_function;
    const double cooling_rate;
    const double energy_change_inactivity_threshold;
    const int n_inactive_iterations_threshold;
    double temperature;

    inline void update_temperature();
    const double step_size;
    const double temperature_coefficient;
    double acceptance_probability(double current_energy, double new_energy);
};

#endif //OPTIMIZATION_SAOPTIMIZER_H
