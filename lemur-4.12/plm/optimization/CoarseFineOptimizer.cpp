#include "CoarseFineOptimizer.h"
#include "utility.h"

#include <limits>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <limits>
#include <stdlib.h>
#include <time.h>

using namespace std;

CoarseFineOptimizer::CoarseFineOptimizer(MultivariateRealFunction *target_function) {
    this->target_function = target_function;
    srand(time(NULL)); //used for creating random solutions
}

Solution CoarseFineOptimizer::optimize(int n_stages, int n_solutions_per_stage) {
    //last config : 8, 12
    vector<Interval> search_domain = target_function->get_domain();
    Solution best_solution_in_stage(target_function), best_overall_solution(target_function);
    double best_energy_in_stage, best_overall_energy = numeric_limits<double>::max();

    for (int j = 0; j < n_stages; j++) {
        cout<<endl<<"stage "<<j<<endl;

        pair<Solution, double> stage_result = run_optimization_stage(search_domain, n_solutions_per_stage);
        best_solution_in_stage = stage_result.first;
        best_energy_in_stage = stage_result.second;

        if (best_energy_in_stage < best_overall_energy) {
            best_overall_energy = best_energy_in_stage;
            best_overall_solution = best_solution_in_stage;
        }

        focus_search_domain_around_solution(best_solution_in_stage, search_domain);
    }

    cout<<endl<<"best overall solution is: "<<best_overall_solution.to_string()<<" "<<best_overall_energy<<endl<<endl;
    cout<<"===================================="<<endl<<endl;

    return best_overall_solution;
}

Solution CoarseFineOptimizer::generate_random_constrained_solution(std::vector<Interval> constraint) {
    vector<double> result(constraint.size());
    for (int i = 0; i < result.size(); i++) {
        result[i] = random_double(constraint[i].start, constraint[i].end);
    }
    return Solution(target_function, result);
}

std::pair<Solution, double> CoarseFineOptimizer::run_optimization_stage(
        std::vector<Interval> search_domain, int n_solutions) {

    double best_solution_value = std::numeric_limits<double>::max();
    Solution best_solution(target_function);

    for (int i = 0; i < n_solutions; i++) {
        Solution new_solution = generate_random_constrained_solution(search_domain);
        double new_solution_value = new_solution.calculate_energy();

        cout<<"random solution " << i << " : "<<new_solution.to_string()<<" "<<new_solution_value<<endl;

        if (new_solution_value < best_solution_value) {
            best_solution_value = new_solution_value;
            best_solution = new_solution;
        }
    }
    cout<<"best stage solution: "<<best_solution.to_string()<<" "<<best_solution_value<<endl;

    return make_pair(best_solution, best_solution_value);
}

void CoarseFineOptimizer::focus_search_domain_around_solution(Solution& focus_point, vector<Interval> &search_domain) {

    for (int i = 0; i < search_domain.size(); i++) {
        double new_interval_length = search_domain[i].get_length() / 2;

        double lower_bound = target_function->get_domain()[i].start;
        double upper_bound = target_function->get_domain()[i].end;

        search_domain[i].start = max(focus_point[i] - new_interval_length / 2, lower_bound);
        search_domain[i].end = min(focus_point[i] + new_interval_length / 2, upper_bound);
    }

    cout<<"new shrunk search domain is: "<<endl;
    for (int i = 0; i <search_domain.size(); i++) {
        cout<< search_domain[i].to_string() << " ";
    }
    cout<<endl;
}

Solution CoarseFineOptimizer::optimize_multiple_times(int n_times, int n_stages, int n_solutions_per_stage) {

    double best_overall_energy = numeric_limits<double>::max();
    Solution best_overall_solution(target_function);

    for (int i = 0; i < n_times; i++) {
        Solution new_solution = optimize(n_stages, n_solutions_per_stage);

        if (new_solution.get_energy() < best_overall_energy) {
            best_overall_energy = new_solution.get_energy();
            best_overall_solution = new_solution;
        }
    }

    return best_overall_solution;
}


//Solution CoarseFineOptimizer::optimize(std::string output_file_name) {
//    ofstream out_file(output_file_name.c_str());
//    return optimize(out_file);
//}
//
//Solution CoarseFineOptimizer::optimize() {
//    out_stream<<"hello"<<endl;
//}
