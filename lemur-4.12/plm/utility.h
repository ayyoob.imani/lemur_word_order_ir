#ifndef _PLM_UTILITY_H_
#define _PLM_UTILITY_H_

#include <vector>

int count_ones(int arr[], int arr_length);
double calculate_weighted_average(std::vector<double> values, std::vector<int> weights);


#endif