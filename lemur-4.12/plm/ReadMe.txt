This is an implementation of the positional language model for ad hoc information retrieval. Please refer to the following paper for more details of the algorithm:

[1] Yuanhua Lv and ChengXiang Zhai. "Positional Language Models for Information Retrieval". In Proceedings of the 32nd Annual International ACM SIGIR Conference on Research and Development in Information Retrieval (SIGIR'09), pages 299-306, 2009.

The algorithm is implemented in C++ and works with the Lemur toolkit (currently not supporting Indri search engine). Most of the codes were used in the experiments for our sigir'09 paper. The algorithm has only been tested on Lemur 4.10 (probably it can also work with othter versions, but we haven't tested it yet) in a Linux environment, where the index type is "key", built using the BuildIndex application provided by Lemur. The current version of the algorithm only "re-ranks" result documents using the PLM, so you need to first retrieve a list of documents using other retrieval models, e.g., language models + Dirichlet prior smoothing method. Note that we did not change any internal implementation of Lemur. As for an experimental system, we haven't yet put too much effort to improve the efficiency, which could be done easily by using an index with term position information in the corresponding documents. 



The PLMRetEval.param file provides some recommended parameter settings. The PLM-specific parameters include 

<!--  Number of documents to be ranked using PLM  -->
<RerankedDocCount>2000</RerankedDocCount>

<!--  Size of the "soft" passage (sigma)  -->
<PLMSigma>175</PLMSigma>

<!-- Propagation function: -1 Passage; 0 Gaussian; 1: Cosine; 2: Triangle; 3: Arc; 4: Circle-->
<PropFunction>0</PropFunction>

<!--  Jelinek-Mercer Smoothing 0; Dirichlet prior Smoothing 1  -->
<PLMSmoothMethod>1</PLMSmoothMethod>
<PLMJMLambda>0.5</PLMJMLambda>
<PLMDirPrior>500</PLMDirPrior>

<!--  The weight of PLM if we interpolate PLM with the original relevance score  -->
<PLMCoefficient>1.0</PLMCoefficient>

<!--  1: do not use PLM for single-term query; 0: otherwise -->
<IgnoreSingleTermQuery>0</IgnoreSingleTermQuery>


Other parameters in the PLMRetEval.param file are standard parameters used in Lemur. For example, you can also do a pseudo relevance feedback after re-ranking documents using PLM. We support standard query format, as shown below:

<DOC 301>
intern
organ
crime
</DOC>
<DOC 302>
poliomyel
post
polio
</DOC>
<DOC 303>
hubbl
telescop
achiev
</DOC>

Please note that we have done stemming and stopword removal for the queries before retrieval experiments.





To run our algorithm, you need to first install the Lemur toolkit, change "prefix = ../Tools/lemur-4.10/install" in the Makefile file to your installation path, and compile the codes.
Finally, you can run our algorithm like this: ./PLMRetEval PLMRetEval.param



If you have more questions, please email me (Yuanhua Lv, ylv2@uiuc.edu)