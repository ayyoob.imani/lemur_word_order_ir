/*
 * FeatureCache.cpp
 *
 *  Created on: Apr 27, 2018
 *      Author: vandermonde
 */

#include "FeatureCache.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include "common_headers.hpp"
#include <math.h>

namespace vandermonde {

void FeatureCache::readCacheFile(std::string cacheFile){
	std::ifstream infile(cacheFile);
	std::string line;

	 if(!infile || !infile.is_open()) {
		cout << "Cannot open cache file. for read\n";
		return;
	 }

	while(std::getline(infile, line)){
		std::string featName;
		std::string key;
		double val;

		int fspace=0;
		int secSpace=0;
		for(int i=0; i< line.size(); i++){
			if(line[i]==' '){
				featName = line.substr(0, i);
				for(int j = i+1; j< line.size(); j++){
					if(line[j] == ' '){
						key=line.substr(i+1, j-i-1);
						val = atoi(line.substr(j+1, line.size()-j-1).c_str());
						break;
					}

				}
				break;
			}
		}

//		cout<<featName<<"#"<< key << "#"<<val<<endl;
		if(cache->find(featName) == cache->end())
			(*cache)[featName] = new std::unordered_map<std::string, double>;

		(*((*cache)[featName]))[key] = val;

	}

}

FeatureCache::FeatureCache(std::string cacheFile, lemur::api::Index *ind, double windowSize) {
	cache = new std::unordered_map<std::string, std::unordered_map<std::string, double>*>();
	readCacheFile(cacheFile);
	this->ind = ind;
	this->windowSize = windowSize;
}

FeatureCache::~FeatureCache() {
	// TODO Auto-generated destructor stub
}

void FeatureCache::saveCache(std::string cacheFile){
	std::ofstream outfile;
	outfile.open(cacheFile.c_str(), std::ofstream::out | std::ofstream::trunc);

	if(!outfile | !outfile.is_open()){
		cerr<<"couldn't open cache file for write\n";
		return;
	}

	for(auto const& it : (*cache)){
		std::string fname = it.first;
		std::unordered_map<std::string, double> *mmap = it.second;

		for(auto const& it2 : (*mmap)) {
			outfile<< fname << " " << it2.first << " " << it2.second << std::endl;
		}

	}

	outfile.close();
}

double calclateFeatureName(std::string featName, std::string key, lemur::api::Index *ind, double windowSize);

string getReversedKey(string key){
	cout<<"key is: "<<key;
	string firstFeat;
	string secFeat;


	for(int i=0; i< key.size(); i++){
		if(key[i]==','){
			firstFeat = key.substr(0, i);
			secFeat = key.substr(i+1, key.size()-i-1);
		}
	}
	string reversedKey = secFeat + "," +firstFeat;
	cout<<" \treversed key is: "<<reversedKey<<endl;
	return reversedKey;
}

double FeatureCache::getValue(std::string featName, std::string key){
	double res;
//	cout<< "searching for: " << featName <<" : "<<key<<endl;
	if(cache->find(featName) == cache->end()){
		(*cache)[featName] = new std::unordered_map<std::string, double>();
		cout<<"feat "<<featName << " notfound!\n";
	}

	string reversedKey;
	if(((*cache)[featName])->find(key) == ((*cache)[featName])->end()){
		if(featName.compare("WPRX") == 0){
			reversedKey = getReversedKey(key);

			if(((*cache)[featName])->find(reversedKey) == ((*cache)[featName])->end()){
				cout<<"key: "<<key<<" not found\n";
				res = calclateFeatureName(featName, key, this->ind, this->windowSize);
				(*(*cache)[featName])[key] = res;
			} else
				res = (*(*cache)[featName])[reversedKey];
		} else{
			cout<<"key: "<<key<<" not found\n";
			res = calclateFeatureName(featName, key, this->ind, this->windowSize);
			(*(*cache)[featName])[key] = res;
		}
	}
	else
		res = (*(*cache)[featName])[key];

	if(res != 0)

		res = log(res);
	return res;
}


double countUniCF(std::string key, lemur::api::Index *ind){
	int res = ind->docCount(ind->term(key.c_str()));
	return res;
}

double countBCF(std::string key, lemur::api::Index *ind){
	unordered_map<lemur::api::DOCID_T, lemur::api::DocInfo*> mmap;
	stringstream ss(key);

	string first;
	string sec;
	std::getline(ss, first, ',');
	std::getline(ss, sec, ',');

	lemur::api::DocInfoList *flist = ind->docInfoList(ind->term(first.c_str()));
	lemur::api::DocInfoList *slist = ind->docInfoList(ind->term(sec.c_str()));

	flist->startIteration();
	while(flist->hasMore()){
		lemur::api::DocInfo * dinfo = flist->nextEntry();
		mmap[dinfo->docID()] = dinfo;
	}

	int res =0;
	slist->startIteration();
	while(slist->hasMore()){
		lemur::api::DocInfo *sinfo = slist->nextEntry();

		if(mmap.find(sinfo->docID()) != mmap.end()){
			for (int i =0; i< sinfo->termCount(); i++)
				for (int j =0; j< mmap[sinfo->docID()]->termCount(); j++)
					if(sinfo->positions()[i] - 1 == mmap[sinfo->docID()]->positions()[j]){
						res ++;
						break;
					}
		}
	}

	delete flist;
	delete slist;

	return res;
}

double countProx(std::string key, lemur::api::Index *ind, double windowSize){
	unordered_map<lemur::api::DOCID_T, lemur::api::DocInfo*> mmap;
	stringstream ss(key);

	string first;
	string sec;
	std::getline(ss, first, ',');
	std::getline(ss, sec, ',');

	lemur::api::DocInfoList *flist = ind->docInfoList(ind->term(first.c_str()));
	lemur::api::DocInfoList *slist = ind->docInfoList(ind->term(sec.c_str()));

	flist->startIteration();
	while(flist->hasMore()){
		lemur::api::DocInfo * dinfo = flist->nextEntry();
		mmap[dinfo->docID()] = dinfo;
	}

	int res =0;
	slist->startIteration();
	while(slist->hasMore()){
		lemur::api::DocInfo *sinfo = slist->nextEntry();

		if(mmap.find(sinfo->docID()) != mmap.end()){
			for (int i =0; i< sinfo->termCount(); i++)
				for (int j =0; j< mmap[sinfo->docID()]->termCount(); j++)
					if(abs(sinfo->positions()[i] - mmap[sinfo->docID()]->positions()[j]) < windowSize){
						res ++;
						break;
					}
		}
	}

	delete flist;
	delete slist;

	return res;

}

double calclateFeatureName(std::string featName, std::string key, lemur::api::Index *ind, double windowSize){
	double res = 0;

	if(strcmp(featName.c_str(), "UCF") == 0)
		res = countUniCF(key, ind);
	else if(strcmp(featName.c_str(), "BCF") == 0)
		res = countBCF(key, ind);
	else if(strcmp(featName.c_str(), "PROX") == 0)
		res = countProx(key, ind, windowSize);
	else {
		cout<<"bad news a wikipedia feature does not exists: "<<featName<<" "<<key<<" "<<endl;
		exit(-1);
	}
	return res;
}

} /* namespace vandermonde */
