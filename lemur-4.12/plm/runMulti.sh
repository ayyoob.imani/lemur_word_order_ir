#!/bin/bash
rm results -rf
mkdir results
#run plm with no order:
sed 's@<resultFile></resultFile>@<resultFile>results/ranks</resultFile>@g' params.xml > params_plm.xml

./PLMRetEval params_plm.xml

coeffs=(0.5 0.8 1.0 2 4 8)
windows=(4 5)
for coeff in ${coeffs[@]}; do
	for window in ${windows[@]}; do
		sed "s@<resultFile></resultFile>@<resultFile>results/ranks_$window$coeff</resultFile>@g" params.xml | sed "s@<orderedPLM>0</orderedPLM>@<orderedPLM>1</orderedPLM>@g" | sed "s@<OrderWindowSize>5</OrderWindowSize>@<OrderWindowSize>$window</OrderWindowSize>@g" | sed "s@<coeff>1</coeff>@<coeff>$coeff</coeff>@g" > "params_plm_$window$coeff.xml"
		./PLMRetEval "params_plm_$window$coeff.xml"
	done
done

