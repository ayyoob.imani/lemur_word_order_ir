/*
 * FeatureCache.h
 *
 *  Created on: Apr 27, 2018
 *      Author: vandermonde
 */

#ifndef PLM_PARAMS_FEATURECACHE_H_
#define PLM_PARAMS_FEATURECACHE_H_

#include <Index.hpp>
#include <string>
#include <unordered_map>
namespace vandermonde {

class FeatureCache {
public:
	FeatureCache(){};
	FeatureCache(std::string cacheFile, lemur::api::Index* ind, double windowsize);
	virtual ~FeatureCache();

	void saveCache(std::string cacheFile);
	double getValue(std::string featName, std::string key);
private:
	void readCacheFile(std::string cacheFile);


	std::unordered_map<std::string, std::unordered_map<std::string, double>*> *cache;
	std::unordered_map<int, std::string> *featNames;
	lemur::api::Index *ind;
	double windowSize;
};

} /* namespace vandermonde */

#endif /* PLM_PARAMS_FEATURECACHE_H_ */
