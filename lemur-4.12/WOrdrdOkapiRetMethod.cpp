/*
 * WOrdrdOkapiRetMethod.cpp
 *
 *  Created on: Jul 5, 2017
 *      Author: vandermonde
 */

#include "WOrdrdOkapiRetMethod.h"
#include <MatchInfo.hpp>
#include <map>

namespace vandermonde {

WOrdrdOkapiRetMethod::WOrdrdOkapiRetMethod(const Index &dbIndex,
		ScoreAccumulator &accumulator, int windowSize) :
		OkapiRetMethod(dbIndex, accumulator) {

	WOAdjFunc = new WOrderOkapiScoreFunc(dbIndex, windowSize);

}

WOrdrdOkapiRetMethod::~WOrdrdOkapiRetMethod() {
	delete WOAdjFunc;
}

const lemur::api::QueryTerm * getQueryTerm(const TextQueryRep *qRep, int tid) {
	qRep->startIteration();
	while (qRep->hasMore()) {
		const lemur::api::QueryTerm * term = qRep->nextTerm();
		if (term->id() == tid)
			return term;
	}

	return NULL;
}

double WOrderOkapiScoreFunc::adjustedScore2(double origScore,
		const TextQueryRep *qRepn, const DocumentRep *dRep, double coeff) const {
	DOCID_T did = dRep->getID();

	vector<const lemur::api::QueryTerm*> query;

	std::map<TERMID_T, MatchInfo*> mymap;

	WOrderOkapiQueryRep* qRep = (WOrderOkapiQueryRep*) qRepn;
	int queryId = atoi(qRep->query->id().c_str());
	lemur::api::TermInfoList *tList = ind.termInfoList(did);

	MatchInfo *m;
	tList->startIteration();
	while (tList->hasMore()) {
		lemur::api::TermInfo* info = tList->nextEntry();
		m = new MatchInfo();
		m->tid = info->termID();
		m->freq = info->count();
		m->poses = new LOC_T[m->freq];
		for (int i = 0; i < m->freq; i++)
			m->poses[i] = info->positions()[i];
		mymap[m->tid] = m;
	}

	//terms in qRep are no in same order as qRep.query, so we iterate over it
	qRep->query->startTermIteration();
	int querySize = 0;
	while (qRep->query->hasMore()) {
		querySize++;
		TERMID_T tid = ind.term(qRep->query->nextTerm()->spelling());

		const QueryTerm* termm = getQueryTerm(qRep, tid);
		if (termm != NULL)
			query.push_back(termm);
	}

	for (int i = 0; i < query.size() - 1; i++)
		for (int j = i + 1; j < query.size() && j < i + windowSize; j++) {

			if (query.at(i)->id() == query.at(j)->id())
				continue;

			bool matched = false;
			MatchInfo *firstMatch;
			MatchInfo *secMatch;

			if (mymap.find(query.at(i)->id()) != mymap.end()) {
				firstMatch = mymap[query.at(i)->id()];

				if (mymap.find(query.at(j)->id()) != mymap.end()) {
					secMatch = mymap[query.at(j)->id()];
					matched = true;
				}
			}

			if (matched) {
				DocInfo * dInfo1 = new DocInfo(did, firstMatch->freq);
				DocInfo *dInfo2 = new DocInfo(did, secMatch->freq);


				double w1 = matchedTermWeight(query.at(i), qRepn, dInfo1, dRep);
				double w2 = matchedTermWeight(query.at(j), qRepn, dInfo2, dRep);

				double ord_mi = ordered_MI(query.at(i)->id(), query.at(j)->id());
//				double revert_mi = ordered_MI(query.at(j)->id(), query.at(i)->id());

				if (areInOrder(firstMatch->poses, firstMatch->freq,
						secMatch->poses, secMatch->freq)) {
					origScore += abs(0.5 - ord_mi) * coeff * (w1 + w2);
				}
//				if (ord_mi <= revert_mi) {
////				cout<<"ord_mi: "<<ord_mi<<"\trevert_mi: "<<revert_mi<<endl;
//					if (areInOrder(firstMatch->poses, firstMatch->freq,
//							secMatch->poses, secMatch->freq)) {
//
//						origScore += abs(ord_mi) * (w1 + w2);
//					} else {
////						origScore += (ord_mi) * (w1 + w2);
//
////						origScore -= log(1 + log(1 + ord_mi * (w1 + w2)));
//					}
//				} else {
//					if (areInOrder(firstMatch->poses, firstMatch->freq,
//							secMatch->poses, secMatch->freq)) {
//
//						origScore += 0.5 * revert_mi * (w1 + w2);
//
//					} else {
////						origScore += revert_mi * (w1 + w2);
////						origScore -= revert_mi * (w1 + w2);
//					}
//				}

			delete dInfo1, dInfo2;
			}
		}

	delete tList;
	for(std::map<TERMID_T, MatchInfo*>::iterator it = mymap.begin(); it != mymap.end(); it++)
		delete (*it).second;
	mymap.clear();
	return origScore;
}

bool WOrderOkapiScoreFunc::areInOrder(const LOC_T* firstPoses, int firstCount,
		const LOC_T* secPoses, int secCount) const {
	for (int i = 0; i < firstCount; i++)
		for (int j = 0; j < secCount; j++)
			if (0 < secPoses[j] - firstPoses[i]
					&& secPoses[j] - firstPoses[i] < windowSize)
				return true;
	return false;
}

void WOrderOkapiScoreFunc::intersectDocs(const LOC_T* firstPoses,
		int firstCount, const LOC_T* secPoses, int secCount,
		int* intersects) const {

	bool direct(false), reverse(false);

	for (int i = 0; i < firstCount; i++)
		for (int j = 0; j < secCount; j++){
			if (0 < firstPoses[i] - secPoses[j] && firstPoses[i] - secPoses[j] < windowSize)
				reverse = true;
			if (0 < secPoses[j] - firstPoses[i] && secPoses[j] - firstPoses[i] < windowSize)
				direct = true;

//			if (abs(firstPoses[i] - secPoses[j]) < minDist) {
//				minDist = abs(firstPoses[i] - secPoses[j]);
//				bestFirst = firstPoses[i];
//				bestSec = secPoses[j];
//			}
		}

	if (reverse)
		intersects[1]++;

	if (direct)
		intersects[0]++;

}

bool WOrderOkapiScoreFunc::inIdfcache(int id) const {
	for (int i = 0; i < idfCache.size(); i++)
		if (idfCache[i] == id) {
			return true;
		}

	return false;
}

bool WOrderOkapiScoreFunc::incache(int first, int sec, double &res) const {
	for (int i = 0; i < cache.size(); i++)
		if (cache[i].first == first && cache[i].second == sec) {
			res = cache[i].MI;
			return true;
		}

	return false;
}

void WOrderOkapiScoreFunc::addtocache(int first, int sec, double val) const {
	cacheItem item;
	item.MI = val;
	item.first = first;
	item.second = sec;

	cache.push_back(item);

	if (cache.size() > cacheSize)
		cache.erase(cache.begin());
}

void WOrderOkapiScoreFunc::addtoIdfcache(int first) const {
	idfCache.push_back(first);

	if (cache.size() > cacheSize)
		cache.erase(cache.begin());
}

double WOrderOkapiScoreFunc::ordered_MI(int first, int sec) const{
	double res;

	if (incache(first, sec, res))
		return res;

	DocInfoList *dl1 = ind.docInfoList(first);
	DocInfoList *dl2 = ind.docInfoList(sec);

	int *intersects = new int[2];
	intersects[0] = intersects[1] = 0;
	int totalFirst = 0;
	int totalSec = 0;

	std::map<DOCID_T, MatchedTermInfo> mymap;
	DocInfo *d2Entry;
	dl2->startIteration();
	while (dl2->hasMore()) {
		d2Entry = dl2->nextEntry();
//		if (FeedbackDocs::isInFeedback(d2Entry->docID(), qid)) {
		totalSec++;
		MatchedTermInfo m;
		m.tid = d2Entry->docID();
		m.freq = d2Entry->termCount();
		m.poses = d2Entry->positions();
		mymap[m.tid] = m;
//		}
	}

// iterate over entries in docList
	dl1->startIteration();

	DocInfo *d1Entry;
	int totalTogether = 0;
	while (dl1->hasMore()) {
		d1Entry = dl1->nextEntry();
//		if (FeedbackDocs::isInFeedback(d2Entry->docID(), qid)) {
		totalFirst++;
		std::map<DOCID_T, MatchedTermInfo>::const_iterator got = mymap.find(
				d1Entry->docID());
		if (got != mymap.end()) {
			totalTogether++;
			MatchedTermInfo m = got->second;
			intersectDocs(d1Entry->positions(), d1Entry->termCount(), m.poses,
					m.freq, intersects);
		}
//		}
	}

	res = (double(intersects[0]) ) / (double(intersects[0] + intersects[1]));
	double rev = 1-res;
	if (intersects[0] + intersects[1] < 10){
		res = 0.5;
		rev = 0.5;
	}

//	res *= pow(2.0, computeMI(totalTogether, totalFirst, totalSec, ind->docCount()));
//	res *= computeMI(totalTogether, totalFirst, totalSec, ind->docCount());


	cout << "mi for: " << ind.term(first).data() << " and " << ind.term(sec)
			<< " is: " << res << endl;

	delete dl1;
	delete dl2;
	delete[] intersects;

	addtocache(first, sec, res);
	addtocache(sec, first, rev);
	return res;
}


//
//double WOrderOkapiScoreFunc::ordered_MI(int first, int sec) const {
//	double res;
//
//	if (incache(first, sec, res))
//		return res;
//
//	DocInfoList *dl1 = ind.docInfoList(first);
//	DocInfoList *dl2 = ind.docInfoList(sec);
//
//	int *intersects = new int[2];
//	intersects[0] = intersects[1] = 0;
//	int totalFirst = 0;
//	int totalSec = 0;
//
//	std::map<DOCID_T, MatchInfo> mymap;
//	DocInfo *d2Entry;
//	dl2->startIteration();
//	while (dl2->hasMore()) {
//		d2Entry = dl2->nextEntry();
////		if (FeedbackDocs::isInFeedback(d2Entry->docID(), qid)) {
//		totalSec++;
//		MatchInfo m;
//		m.tid = d2Entry->docID();
//		m.freq = d2Entry->termCount();
//		m.poses = new LOC_T[m.freq];
//		for (int i = 0; i < m.freq; i++)
//			m.poses[i] = d2Entry->positions()[i];
//		mymap[m.tid] = m;
////		}
//	}
//
//// iterate over entries in docList
//	dl1->startIteration();
//
//	DocInfo *d1Entry;
//	int totalTogether = 0;
//	while (dl1->hasMore()) {
//		d1Entry = dl1->nextEntry();
////		if (FeedbackDocs::isInFeedback(d2Entry->docID(), qid)) {
//		totalFirst++;
//		std::map<DOCID_T, MatchInfo>::const_iterator got = mymap.find(
//				d1Entry->docID());
//		if (got != mymap.end()) {
//			totalTogether++;
//			MatchInfo m = got->second;
//			intersectDocs(d1Entry->positions(), d1Entry->termCount(), m.poses,
//					m.freq, intersects);
//		}
////		}
//	}
//
//	res = (double(intersects[0])) / double(totalTogether);
//
//	cout << "mi for: " << ind.term(first).data() << " and " << ind.term(sec)
//			<< " is: " << res << endl;
//
//	delete dl1;
//	delete dl2;
//	delete[] intersects;
//
//	addtocache(first, sec, res);
//	return res;
//}

}
