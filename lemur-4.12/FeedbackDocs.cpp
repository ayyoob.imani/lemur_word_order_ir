/*
 * FeedbackDocs.cpp
 *
 *  Created on: Oct 16, 2017
 *      Author: vandermonde
 */

#include "FeedbackDocs.h"

namespace vandermonde {

FeedbackDocs::FeedbackDocs(){
	// TODO Auto-generated constructor stub
}

FeedbackDocs::~FeedbackDocs() {
	// TODO Auto-generated destructor stub
}

map<lemur::api::DOCID_T, int> FeedbackDocs::fbdocs;

void FeedbackDocs::setup(lemur::api::IndexedRealVector ir, int qid, int count) {
	FeedbackDocs::fbdocs.clear();
	lemur::api::IndexedRealVector::iterator it;
	int counter = 0;
	for(it = ir.begin(); it != ir.end() && counter < count; it++, counter++){
		fbdocs[qid] = (*it).ind;
	}
}

bool FeedbackDocs::isInFeedback(lemur::api::DOCID_T did, int qid) {
	map<lemur::api::DOCID_T, int>::iterator it;
	it = fbdocs.find(did);
	if(it != fbdocs.end() && (*it).second == qid)
		return true;

	return false;
}

} /* namespace vandermonde */
