/*
 * JudgmentReader.h
 *
 *  Created on: Dec 29, 2016
 *      Author: vandermonde
 */

#ifndef JUDGMENTREADER_H_
#define JUDGMENTREADER_H_

#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <cstdlib>

#include "utility.h"

using namespace std;
using namespace myutility;


class Judgment{
public:
	string doc_id;
	int q_id;
	bool rel;
};

//class FileNotFoundException: public exception
//{
//	string cause;
//  virtual const char* what() const throw()
//  {
//    return cause.c_str();
//  }
//
//  FileNotFoundException(string cause){
//	  this->cause = cause;
//  }
//
//};

class JudgmentReader {


public:
	map<int, map<string, bool> > data_by_qid;
	JudgmentReader(const std::string & file_name);
	virtual ~JudgmentReader();

};

#endif /* JUDGMENTREADER_H_ */
