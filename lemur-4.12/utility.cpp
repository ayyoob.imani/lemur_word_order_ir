/*
 * utility.cpp
 *
 *  Created on: Dec 29, 2016
 *      Author: vandermonde
 */

#include "utility.h"

namespace myutility {
// Left trim the given string ("  hello!  " --> "hello!  ")
string left_trim(string str) {
	int numStartSpaces = 0;
	for (int i = 0; i < str.length(); i++) {
		if (!isspace(str[i]))
			break;
		numStartSpaces++;
	}
	return str.substr(numStartSpaces);
}

// Right trim the given string ("  hello!  " --> "  hello!")
string right_trim(string str) {
	int numEndSpaces = 0;
	for (int i = str.length() - 1; i >= 0; i--) {
		if (!isspace(str[i]))
			break;
		numEndSpaces++;
	}
	return str.substr(0, str.length() - numEndSpaces);
}

// Left and right trim the given string ("  hello!  " --> "hello!")
string trim(string str) {
	return right_trim(left_trim(str));
}
}
