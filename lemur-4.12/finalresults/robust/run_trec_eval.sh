#!/bin/bash

# first param is address of files
#second param is judg file
echo "$1"
echo "$2"

files=`ls $1`

res_withQ="$2_queries_map"
res="$2_map"
echo "" > "$res_withQ"
echo "" > "$res"

for file in $files
do
echo "$file" >> "$res_withQ"
echo "-----------------------------------------------------------------------------" >> "$res_withQ"
echo "$file" >> "$res"
echo "-----------------------------------------------------------------------------" >> "$res"
echo "../../trec_eval/trec_eval -q $2 $1/$file | grep map | awk '{print $3}' >> $res_withQ"

echo "../../trec_eval/trec_eval $2 $1/$file | grep map >> $res"


../../trec_eval/trec_eval -q "$2" "$1/$file" | grep map | awk '{ print $3}' >> "$res_withQ"
../../trec_eval/trec_eval "$2" "$1/$file" | grep map >> "$res"
../../trec_eval/trec_eval "$2" "$1/$file" | grep "P_10" >> "$res"

echo "==============================================================================" >>"$res"
echo "==============================================================================" >>"$res_withQ"
done
