/*
 * MyResFile.h
 *
 *  Created on: May 17, 2018
 *      Author: vandermonde
 */

#ifndef RUNSTAT_MYRESFILE_H_
#define RUNSTAT_MYRESFILE_H_

#include <ResultFile.hpp>
using namespace lemur::api;

namespace vandermonde {




class MyResFile {
  public:
    MyResFile(bool TRECFormat = true) : trecFmt (TRECFormat), resTable(NULL) {
      resTable = new lemur::utility::ISet<lemur::utility::ResultEntry>(50);
    }

    ~MyResFile() { delete resTable;}

    /// Open and associate an input stream for reading, e.g., with getResult function
    void openForRead(istream &is, Index &index);

    /// Load all the results into an internal hash table, so as to allow random access to any of the results.
    void load(istream &is, Index &index);

    /// Read the results for a given query from the associated input stream into memory (stored in res), sequential reading, so appropriate order must be maintained and attempting to get the results for a query that has no results will fail.
    void getResult(const string& expectedQID, IndexedRealVector &res);

    /// Find the results for the given query id, the output variable res gets a pointer to the results, returns true iff found.
    bool findResult(const string& queryID, IndexedRealVector *&res);

    /// Associate an output stream for writing results
    void openForWrite( ostream &os, Index &index) {

      outStr = &os;
      ind = &index;
    }

    /// writing the results (stored in <tt> results</tt>) into the associated output stream, up to a maximum count.

    void writeResults(const string& queryID, IndexedRealVector *results, int maxCountOfResult);

  private:

    bool readLine();

    bool trecFmt;

    //  char curQID[300];
    string curQID;
    char curDID[300];
    double curSC;
    Index *ind;
    istream *inStr;
    ostream *outStr;
    bool eof;

    lemur::utility::ISet<lemur::utility::ResultEntry> * resTable;

  };

} /* namespace vandermonde */

#endif /* RUNSTAT_MYRESFILE_H_ */
