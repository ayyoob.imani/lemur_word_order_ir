/*
 * TermPositionFeature.h
 *
 *  Created on: Jan 2, 2017
 *      Author: vandermonde
 */

#ifndef TERMPOSITIONFEATURE_H_
#define TERMPOSITIONFEATURE_H_
#include <TextQueryRetMethod.hpp>

using namespace lemur::api;
using namespace lemur::retrieval;

namespace wink {

class TermPositionFeature {
public:
	TermPositionFeature(lemur::api::Index *dbIndex,
			lemur::api::ScoreAccumulator &accumulator) :
			scAcc(accumulator), ind(dbIndex) {
	}
	;
	virtual ~TermPositionFeature() {
	}

	void scoreCollection(QueryRep &qRep, IndexedRealVector &scores);

private:
	double computeCorrelation(LOC_T* firstTermPositions, COUNT_T firstTermCount,
			DocInfoList *secondTermdocInfoList, DOCID_T id);
	lemur::api::Index *ind;
	ScoreAccumulator &scAcc;
};

} /* namespace wink */

#endif /* TERMPOSITIONFEATURE_H_ */
