/*
 * FeedbackDocs.h
 *
 *  Created on: Oct 16, 2017
 *      Author: vandermonde
 */

#ifndef FEEDBACKDOCS_H_
#define FEEDBACKDOCS_H_

#include <IndexedReal.hpp>
#include <IndexTypes.hpp>

namespace vandermonde {

class FeedbackDocs {
public:
	FeedbackDocs();
	virtual ~FeedbackDocs();

	static void setup(lemur::api::IndexedRealVector ir, int qid, int count);
	static bool isInFeedback(lemur::api::DOCID_T did, int qid);

private:
	static map<lemur::api::DOCID_T, int> fbdocs;
};

} /* namespace vandermonde */

#endif /* FEEDBACKDOCS_H_ */
