/*
 * DataExtraction.h
 *
 *  Created on: Jul 14, 2017
 *      Author: vandermonde
 */

#ifndef DATAEXTRACTION_H_
#define DATAEXTRACTION_H_

#include <stdio.h>
#include <stdlib.h>
#include <Index.hpp>
#include <IndexManager.hpp>
#include <DocStream.hpp>
#include <BasicDocStream.hpp>
#include <ScoreAccumulator.hpp>
#include <RetrievalMethod.hpp>
#include <TFIDFRetMethod.hpp>
#include <OkapiRetMethod.hpp>
#include <RetMethodManager.hpp>
#include <iostream>
#include <fstream>


#include "WOrdrdOkapiRetMethod.h"
#include "MyResFile.h"

using namespace lemur;
using namespace api;
using namespace parse;
using namespace retrieval;


namespace vandermonde {

class statistics {
public:
	statistics(){
		nonRelordCount = 0, nonRelreverseCount = 0, nonRelordDocCount = 0,
					nonRelreverseDocCount = 0, firstDF = 0, secDF = 0, bothDF = 0,
					nonRelfirstDF = 0, nonRelsecDF = 0, nonRelbothDF = 0;

		relOrdCount = 0, relReverseCount = 0, relOrdDocCount = 0,
					relReverseDocCount = 0, relFirstDF = 0, relSecDF = 0, relbothDF = 0;
	}
	int nonRelordCount , nonRelreverseCount , nonRelordDocCount ,
			nonRelreverseDocCount , firstDF , secDF , bothDF ,
			nonRelfirstDF , nonRelsecDF , nonRelbothDF ;

	int relOrdCount , relReverseCount , relOrdDocCount ,
			relReverseDocCount , relFirstDF , relSecDF , relbothDF ;
};

class DataExtraction {
public:
	DataExtraction();
	virtual ~DataExtraction();

	static void intersectDocs(const LOC_T* firstPoses, int firstCount,
			const LOC_T* secPoses, int secCount, bool isrel,
			statistics &stats) {

		int bestFirst = firstPoses[0];
		int bestSec = secPoses[0];
		int minDist = 999999999;

		for (int i = 0; i < firstCount; i++)
			for (int j = 0; j < secCount; j++) {
				if (0 < firstPoses[i] - secPoses[j]
						&& firstPoses[i] - secPoses[j] < 5) {

					if (isrel)
						stats.relReverseCount++;
					else
						stats.nonRelreverseCount++;
				}
				if (0 < secPoses[j] - firstPoses[i]
						&& secPoses[j] - firstPoses[i] < 5) {
					if (isrel)
						stats.relOrdCount++;
					else
						stats.nonRelordCount++;
				}

				if (abs(firstPoses[i] - secPoses[j]) < minDist) {
					minDist = abs(firstPoses[i] - secPoses[j]);
					bestFirst = firstPoses[i];
					bestSec = secPoses[j];
				}
			}

		if (0 < bestFirst - bestSec && bestFirst - bestSec < 5) {
			if (isrel)
				stats.relReverseDocCount++;
			else
				stats.nonRelreverseDocCount++;
		} else if (0 < bestSec - bestFirst && bestSec - bestFirst < 5) {
			if (isrel)
				stats.relOrdDocCount++;
			else
				stats.nonRelordDocCount++;
		}

	}

	static statistics calculateCollectionNumbers(
			map<DOCID_T, MatchInfo*> &firstMatchInfo,
			map<DOCID_T, MatchInfo*> &secMatchInfo, const Index& ind,
			map<DOCID_T, int> &relevants, bool considerRetreiveds) {

		statistics res;

		res.firstDF = firstMatchInfo.size();
		res.secDF = secMatchInfo.size();
//		for (map<DOCID_T, MatchInfo*>::iterator it = secMatchInfo.begin();
//						it != secMatchInfo.end(); it++) {
////					cout<<"sec "<<(*it).first<<endl;
//		}
		for (map<DOCID_T, MatchInfo*>::iterator it = firstMatchInfo.begin();
				it != firstMatchInfo.end(); it++) {
//			cout<<"first "<<(*it).first<<endl;

			map<DOCID_T, int>::iterator isRel = relevants.find((*it).first);
			map<DOCID_T, MatchInfo*>::iterator inBoth = secMatchInfo.find(
					(*it).first);

			if (isRel == relevants.end()) {
				res.nonRelfirstDF++;
				if (inBoth != secMatchInfo.end()) {
					res.nonRelbothDF++;
					intersectDocs((*it).second->poses, (*it).second->freq,
							(*inBoth).second->poses, (*inBoth).second->freq,
							0, res);
				}

			} else {

				res.relFirstDF++;
				if (inBoth != secMatchInfo.end()) {
					res.relbothDF++;
					intersectDocs((*it).second->poses, (*it).second->freq,
							(*inBoth).second->poses, (*inBoth).second->freq,
							1, res);
				}
			}
		}

		return res;
	}

	static map<DOCID_T, MatchInfo*> getTermDocsMap(Index *dbIndex, TERMID_T t) {
		map<DOCID_T, MatchInfo*> res;
		DocInfoList* dinfol = dbIndex->docInfoList(t);
		if (dinfol == NULL)
			return res;
		dinfol->startIteration();
		while (dinfol->hasMore()) {
			DocInfo * dinfo = dinfol->nextEntry();
			vandermonde::MatchInfo *m = new MatchInfo;
			m->freq = dinfo->termCount();
			m->poses = new LOC_T[m->freq];
			for (int i = 0; i < m->freq; i++)
				m->poses[i] = dinfo->positions()[i];
			m->tid = t;

			res[dinfo->docID()] = m;
		}

		delete dinfol;

		return res;
	}

	static void extractAndWriteData(string index, string query, string judgment) {
		cout << "firstTerm,"
				<< "secTerm,"
				<<"firstDF" << "," << "secDF" << ","
				<< "relFirstDF" << "," << "nonRelFirstDF" << ","
				<< "relbothDF" << "," << "nonRelbothDF" << ", "
				<< "relOrdCount" << ","
				<< "nonRelordCount" << ","
				<< "relReverseCount" << ","
				<< "nonRelreverseCount" << ","
				<< "relOrdDocCount" << ","
				<< "nonRelordDocCount" << ","
				<< "relReverseDocCount" << ","
				<< "nonRelreverseDocCount" << ","
				<< "relevantSize" << "," << "termsSize" << ","
				<< "distance" << endl;

		Index *dbIndex = IndexManager::openIndex(index.c_str());
		std::filebuf fb;
//		cout<<"openeing jud file: "<< judgment.c_str()<< endl;
		fb.open(judgment.c_str(), std::ios::in);
		std::istream is(&fb);
		MyResFile ResFile;

		ResFile.openForRead(is, *dbIndex);



		DocStream *queryStream = new BasicDocStream(
				query.c_str());

		queryStream->startDocIteration();
		TextQuery *q;
		int count = 51;

		while (queryStream->hasMore()) {
			Document *d = queryStream->nextDoc(); // fetch the query document
			q = new TextQuery(*d);
			vector<map<DOCID_T, MatchInfo*> > termDocs;
			vector<TERMID_T> terms;

			q->startTermIteration();
			if(atoi(q->id().c_str()) == 65){
				while (q->hasMore())
					TERMID_T t = dbIndex->term(q->nextTerm()->spelling());

				continue;
			}

			while (q->hasMore()) {
				TERMID_T t = dbIndex->term(q->nextTerm()->spelling());


				terms.push_back(t);
				termDocs.push_back(getTermDocsMap(dbIndex, t));
			}

			IndexedRealVector relevant;
			ResFile.getResult(q->id(), relevant);
			map<DOCID_T, int> rels;
			for (IndexedRealVector::iterator it = relevant.begin();
					it != relevant.end(); it++)
				if ((*it).val){
					rels[(*it).ind] = 1;
//					cout<<"rel  "<<(*it).ind<< endl;
				}

			for (int i = 0; i < terms.size() - 1; i++) {
				for (int j = i + 1; j < terms.size(); j++) {
					if(terms[i] == terms[j])
						continue;
					int distance = j - i ;

					if (distance < 5) {

						statistics stats = calculateCollectionNumbers(
								termDocs[i], termDocs[j], *dbIndex, rels, false);


						cout << dbIndex->term(terms[i]).data() << ","
								<< dbIndex->term(terms[j]).data() << ","
								<< stats.firstDF << "," << stats.secDF << ","
								<< stats.relFirstDF << "," << stats.nonRelfirstDF << ","
								<< stats.relbothDF << "," << stats.nonRelbothDF << ","
								<< stats.relOrdCount << ","
								<< stats.nonRelordCount << ","
								<< stats.relReverseCount << ","
								<< stats.nonRelreverseCount << ","
								<< stats.relOrdDocCount << ","
								<< stats.nonRelordDocCount << ","
								<< stats.relReverseDocCount << ","
								<< stats.nonRelreverseDocCount << ","

								<< relevant.size() << "," << terms.size() << ","
								<< distance << endl;
					}

				}
			}
		}
	}
};

} /* namespace vandermonde */

#endif /* DATAEXTRACTION_H_ */
