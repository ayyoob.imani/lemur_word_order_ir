/*
 * TermPositionFeature.cpp
 *
 *  Created on: Jan 2, 2017
 *      Author: vandermonde
 */

#include "TermPositionFeature.h"
#include <DocInfoList.hpp>

namespace wink {

double TermPositionFeature::computeCorrelation(LOC_T* firstTermPositions, COUNT_T firstTermCount, DocInfoList *secondTermdocInfoList, DOCID_T id){
	double res = 0;
	secondTermdocInfoList->startIteration();
	DocInfo* secdTermDoc;
	LOC_T* secondTermPositions;
	COUNT_T secTermCount;
	bool found = false;
	while(secondTermdocInfoList->hasMore()) {
		secdTermDoc = secondTermdocInfoList->nextEntry();
		if (id == secdTermDoc->docID()){
			found = true;
			secondTermPositions = (int*)secdTermDoc->positions();
			secTermCount = secdTermDoc->termCount();
			break;
		}

	}

	for(int i =0; i<firstTermCount; i++){
		for (int j=0; j<secTermCount; j++)
			res += 1.0/double(abs(firstTermPositions[i] - secondTermPositions[j]));
	}

	return res;
}

void TermPositionFeature::scoreCollection(QueryRep &qRep,
		IndexedRealVector &scores) {

	scAcc.reset();
	int i;
	TextQueryRep *textQR = (TextQueryRep *) (&qRep);

	bool iterationFinished = false;
	int firstTermNumber = 1;
	while (true) {

		textQR->startIteration();
		QueryTerm *firstqTerm;
		for (int i = 0; i < firstTermNumber; i++) {
			if (!textQR->hasMore()) {
				iterationFinished = true;
				break;
			}
			firstqTerm = textQR->nextTerm();
			if (!textQR->hasMore()) {
				iterationFinished = true;
				break;
			}

			if (i<firstTermNumber-1)
				delete firstqTerm;
		}
		if (iterationFinished)
			break;
		DocInfoList *dList = ind->docInfoList(firstqTerm->id());
		while (textQR->hasMore()) {
			QueryTerm *secondqTerm = textQR->nextTerm();
			dList->startIteration();
			while (dList->hasMore()) {
				DocInfo *info = dList->nextEntry();
				DOCID_T id = info->docID();
				DocInfoList * secondTermDocInfo = ind->docInfoList(
						secondqTerm->id());

				scAcc.incScore(id,
						computeCorrelation((int*)info->positions(), info->termCount(), secondTermDocInfo, id) );
				delete secondTermDocInfo;
			}
			delete secondqTerm;

		}
		delete dList;
		firstTermNumber++;
	}

	scores.clear();
	double s;
	for (i = 1; i <= ind->docCount(); i++) {
		scAcc.findScore(i, s);
		scores.PushValue(i, s);
	}
}
} /* namespace wink */
